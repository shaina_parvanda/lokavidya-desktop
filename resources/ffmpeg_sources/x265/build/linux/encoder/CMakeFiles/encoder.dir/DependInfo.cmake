# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/frg/ffmpeg_sources/x265/source/encoder/analysis.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/analysis.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/api.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/api.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/bitcost.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/bitcost.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/dpb.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/dpb.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/encoder.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/encoder.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/entropy.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/entropy.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/frameencoder.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/frameencoder.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/framefilter.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/framefilter.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/level.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/level.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/motion.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/motion.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/nal.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/nal.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/ratecontrol.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/ratecontrol.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/reference.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/reference.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/sao.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/sao.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/search.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/search.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/sei.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/sei.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/slicetype.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/slicetype.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/encoder/weightPrediction.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/encoder/CMakeFiles/encoder.dir/weightPrediction.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "EXPORT_C_API=1"
  "HAVE_INT_TYPES_H=1"
  "HIGH_BIT_DEPTH=0"
  "X265_ARCH_X86=1"
  "X265_DEPTH=8"
  "X265_NS=x265"
  "X86_64=1"
  "__STDC_LIMIT_MACROS=1"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/frg/ffmpeg_sources/x265/source/."
  "/home/frg/ffmpeg_sources/x265/source/common"
  "/home/frg/ffmpeg_sources/x265/source/encoder"
  "."
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
