package Dialogs;

import gui.Call;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/** Getting a Rectangle of interest on the screen.
Requires the MotivatedEndUser API - sold separately. */
public class SelectArea {

	
	Toolkit tk = Toolkit.getDefaultToolkit();
	
	
    Dimension d = tk.getScreenSize();
	public JFrame frame;
	public Cursor c;
	JPanel panel;
    Rectangle captureRect;
    BufferedImage screen,old;
    SelectArea(final BufferedImage screen1) {

    	frame=new JFrame("Select Recording Area");

    	
    	frame.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("resources/drag.png").getImage(),new Point(0,0),"custom cursor"));

    	
    	
    	try{
    		Insets insets=frame.getInsets();
    		System.out.println(insets.top);
        	Rectangle crop=new Rectangle(0,insets.top ,d.width ,(d.height-insets.top));
            screen=cropImage(screen1,crop);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        panel = new JPanel(new BorderLayout()) {
            @Override
            protected void paintComponent(Graphics g) {


                super.paintComponent(g);
                g.drawImage(screen, 0, 0, null);
                if(captureRect!=null){
                	g.setColor(Color.RED);
                	g.drawRect(captureRect.x,captureRect.y,captureRect.width,captureRect.height);
                	//frame.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("drag.png").getImage(),new Point(frame.getWidth(),frame.getHeight()),"custom cursor"));
                	System.out.println("Called rectangle");
                }
                System.out.println("Called method");
            }
           
        };

        
        panel.addMouseMotionListener(new MouseMotionAdapter() {

            Point start = new Point();

            @Override
            public void mouseMoved(MouseEvent me) {
            	//panel.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("/resources/drag.png").getImage(),new Point(800,800),"custom cursor"));
            	
                start = me.getPoint();
             
            }

            @Override
            public void mouseDragged(MouseEvent me) {


                Point end = me.getPoint();
                captureRect = new Rectangle(start.x,start.y,(end.x-start.x), (end.y-start.y));
                panel.repaint();
            }
        });
        


        
        
        
        panel.addMouseListener(new MouseListener() {
			

			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				//set x,y,width,height
				Call.workspace.setCoordinates(captureRect.x,captureRect.y ,captureRect.width ,captureRect.height );
				frame.dispose();
				Call.workspace.setVisible(true);
				Call.workspace.toFront();
			}
			
		
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		
			public void mouseEntered(MouseEvent e) {
			
				// TODO Auto-generated method stub
				
			}
			
	
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
        
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panel.setEnabled(true);
        panel.setVisible(true);
        panel.setOpaque(false);
        
        frame.setUndecorated(true);
        frame.setBackground(new Color(255, 0, 0, 7));
        frame.setVisible(true);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        
        
        
        //RecordingFrame captureRect = new RecordingFrame(0,0,800,600);
        //System.out.println("Rectangle of interest: " + captureRect);
    }

 
    private BufferedImage cropImage(BufferedImage src, Rectangle rect) {
        BufferedImage dest = src.getSubimage(rect.x, rect.y, rect.width, rect.height);
        return dest;
     }
    public static void main(String[] args) throws Exception {
        Robot robot = new Robot();
        final Dimension screenSize = Toolkit.getDefaultToolkit().
                getScreenSize();
        final BufferedImage screen = robot.createScreenCapture(
                new Rectangle(screenSize));

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new SelectArea(screen);
            }
        });
    }
}
