package Xuggler;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.iitb.lokavidya.core.utils.FFMPEGWrapper;

import gui.Call;

public class VideoCapture implements Runnable
{
	Thread thread;
	String outFile;
	CaptureScreenToFile videoEncoder;
	public VideoCapture()
	{
		
	}
	public void addFile(String x)
	{
		outFile=x;
	}
    public void start() {
	  
      thread = new Thread(this);
      if(thread==null)
    	  System.out.println("Thread starting off as null");
      //thread.setName("Capture");
      else
    	  thread.start();
    }

    public void stop() {
      thread = null;
      
      System.out.println("Stopping thread");
      
    }

    private void shutDown(String message) {
      if (message != null && thread != null) {
    	
        thread = null;
        
        System.err.println(message);
      }
    }
    
    public void run()
    {
      

      try
      {
    	  File tempFolder=new File(System.getProperty("java.io.tmpdir"),"ScreenRec");
		  tempFolder.mkdir();  
    	videoEncoder = new CaptureScreenToFile(outFile);
        while (thread!=null)
        {
          if (!Call.workspace.paused) {
			//System.out.println("encoded image");
			/*File outputfile;
			int ctr=1;
			try{
			  String no=Integer.toString(ctr);
			  while(no.length()<4)
			  {
				  no="0"+no;
			  }
			  
			  outputfile = new File(tempFolder.getAbsolutePath(),("img"+no+".png"));
			    ImageIO.write(videoEncoder.takeSingleSnapshot(), "png", outputfile);
			} catch (IOException e) {
			    e.printStackTrace();
			}*/
			videoEncoder.encodeImage(videoEncoder.takeSingleSnapshot());
			// sleep for framerate milliseconds
			Thread.sleep(videoEncoder.getDuration());
		}
          
          
        }
        videoEncoder.closeStreams();
       //new FFMPEGWrapper().encodeImages(tempFolder.getAbsolutePath(), outFile,Integer.toString(videoEncoder.getFps()));
      }
      catch (Exception e)
      {
    	  System.out.println("RunTime Exception");
      	shutDown(e.getMessage());
      }
    }
}