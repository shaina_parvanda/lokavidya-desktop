package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.border.Border;

import com.iitb.lokavidya.core.data.Segment;

public class CustomPanel extends JPanel implements MouseListener
{
    boolean isHighlighted;
    public static int highlightCount;
    public static  int slideCount;
    public int id;
    public static CustomPanel selected[] = new CustomPanel[2];
    Border blackBorder = BorderFactory.createLineBorder(Color.BLACK);
    Border blueBorder = BorderFactory.createLineBorder(Color.BLUE,5);
    public Segment segment;
    public SpringLayout springLayout;
    public void init() {
    	
    }
    
    
    CustomPanel(Segment segment)
    {
    	this.setBackground(new Color(245,245,245));
    	springLayout = new SpringLayout();
    	setLayout(springLayout);
    	this.setSize(207, 152);
    	this.setPreferredSize(new Dimension(207, 147));
    	//this.setMaximumSize();
    	id = slideCount;
    	++slideCount;
    	addMouseListener(this);
        setBorder(blackBorder);
        setFocusable(true);
        this.segment = segment;
    }
//
//    @Override
//    public Dimension getPreferredSize()
//    {
//        return new Dimension(200, 100);
//    }

 public void mouseClicked(MouseEvent e)
    {
        if(isHighlighted)
        {
        	setBorder(blackBorder);
        	if (highlightCount > 0){
        		--highlightCount;
        		isHighlighted=!isHighlighted;
        	}
        }
        else if (!isHighlighted && highlightCount < 2) {
        	setBorder(blueBorder);
        	selected[highlightCount]=this;
        	++highlightCount;
            isHighlighted=!isHighlighted;
        }
    }

    public Segment getSegment() {
    	return this.segment;
    }
    

     public void mousePressed(MouseEvent e){}
  public void mouseReleased(MouseEvent e){}
     public void mouseEntered(MouseEvent e){}
    public void mouseExited(MouseEvent e){}
}
