package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
//import com.sun.star.awt.MouseEvent;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
//import com.sun.star.awt.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import java.util.regex.Pattern;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
//import com.sun.star.awt.MouseEvent;
import javax.swing.Timer;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.SoftBevelBorder;

import libreoffice.LibreDesktop;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;

import Dialogs.CreateProject;
import Dialogs.DirectoryChooser;
import Dialogs.Feedback;
//import Dialogs.Feedback;
import Dialogs.OpenAndroid;
import Dialogs.OpenAndroidExp;
import Dialogs.OpenPresentation;
import Dialogs.OpenProject;
import Dialogs.OpenVideo;
import Dialogs.SelectArea;
import Dialogs.VideoFormat;

import com.iitb.lokavidya.core.data.Project;
import com.iitb.lokavidya.core.data.Segment;
import com.iitb.lokavidya.core.data.State;
import com.iitb.lokavidya.core.data.Video;
import com.iitb.lokavidya.core.operations.ProjectOperations;
import com.iitb.lokavidya.core.operations.ProjectService;
import com.iitb.lokavidya.core.operations.SegmentService;
import com.iitb.lokavidya.core.utils.GeneralUtils;
import com.iitb.lokavidya.core.utils.UIUtils;

public class Workspace extends JFrame {

	public JPanel contentPane, presentationInnerPanel, outputInnerPanel, timelineInnerPanel, videosInnerPanel;
	public JInternalFrame notesFrame, explorerFrame, timelineFrame, slideFrame;
	public JComponent presentationPanel;
	private JComponent outputPanel;
	private JComponent videosPanel;
	private JMenuBar menuBar;
	public static RecordingFrame recFrame;
	private JMenu mnFile, mnEdit, mnOptions, mnExport, mnImport, mnHelp;
	private JMenuItem mntmNewMenuItem, mntmOpenMenuItem, mntmEditPres, mntmVideoFormat, mntmAudioFormat,
			mntmFormat, mntmAndroidexp, mntmPresentation, mntmVideo, mntmAndroid, mntmFeedback;
	private JCheckBoxMenuItem  mntmDecideRecordingArea;
	private JPanel panel;
	private JTabbedPane tabbedPane;
	public JButton btnRecord,btnDelete,btnRefresh;
	public static String path, location, name;
	public static ArrayList<CustomPanel> customPanelList; 		
	public static XMLSlideShow currentPptx;
	public static SlideShow currentPpt;
	public static List<XSLFSlide> currentPptSlide;
	public static int x = 0, y = 0, recordingWidth = 0, recordingHeight = 0,videoWidth=320,videoHeight=240;
	private String pptPath;
	public boolean continuous = true;
	public static boolean paused=false;
	public boolean pausedFile=false;
	public static boolean screenRecordingFlag=false;
	public static boolean playing=false;
	public List<File> deleteList;
	public int framerate=3;
	private static JLabel lblSlideDisplay;
	int currentSlideNumber = 0;
	public static LibSlide currentSlide=null;
	public JButton stopbtn;
	public JButton btnNext;
	private JScrollPane scrollPane;
	private JScrollPane timelineScroll;
	public JButton btnSaveOrder;
	public JButton btnStitch;
	public JButton btnSwap;
	public JButton btnDiscard;
	private JButton btnAdd;
	public static Project currentProject;
	public static Segment currentSegment;
	public List<Segment> refreshList;
	private static ProjectOperations po;
	public JButton btnScreenRec;
	public JTextArea notesArea;
	public JLabel notesLabel_1,notesLabel_2;
	public JButton btnNewButton;
	private JButton btnNewButton_1;
	private String pathDef;
	private JTextField textField;
	public JMenuItem mntmClose;
	public boolean cancelled=false;
	public Stack<State> executedActions,redoActions;
	public State currentState; 
	private JButton btnNewButton_2;
	
	/**
	 * Launch the application.
	 */
	

	public void startOperation()
	{
		executedActions.push(new State(currentState));
		currentState=new State();
	}
	public void initializeStates()
	{
		executedActions.clear();
		redoActions.clear();
		currentState=new State();
	}
	public static void changeSlideRight() {
		try{
			paused=false;
		WorkspaceUIHelper.disableRecord();
		List <Segment> segmentList = currentProject.getOrderedSegmentList();
		int position = segmentList.indexOf(currentSegment);
		if ((position+1) != segmentList.size()) {
			
			if(screenRecordingFlag)
			{
				Thread.sleep(1600);
				po.stopToggleSlideRecording(currentProject);
				setPreview(Integer.toString(position+1));
				po.startToggleSlideRecording(currentProject, currentSegment);
			}
			else
			{
				Thread.sleep(1600);
				ProjectOperations.stopSlideRecording();
				setPreview(Integer.toString(position+1));
				ProjectOperations.startSlideRecording(currentProject, currentSegment);
			}
		}
		else {
			stopRecording();
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
/*
	public static void changeSlideLeft() {
		for (Slide libraryslide : librarySlides) {
			if (libraryslide.getName().equals(currentSlide.getName())) {
				int index = librarySlides.indexOf(libraryslide);
				if ((index - 1) >= 0) {
					setPreview(librarySlides.get(index - 1).getName());
					recording.stopSlide();
					recording.startSlide();
				} else {
					recording.stopSlide();
				}
				break;
			}
		}

	}*/

	public void showOutput() {
		tabbedPane.setSelectedIndex(2);
		
	}
	public void initCustomPanel() {
		CustomPanel.selected[0]=null;
		CustomPanel.selected[1]=null;
		CustomPanel.highlightCount =0;
		CustomPanel.slideCount=0;
		customPanelList.clear();
	}

	
	public void makeExplorerVideo(final Video v) {
		
		// Add to individualrecordings
		String vname=FilenameUtils.getBaseName(v.getVideoURL());
		JLabel l = new JLabel(vname);
		l.setName(vname);
		l.setIcon(createImageIcon("resources/individual_video.png"));
		l.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				String f = e.getComponent().getName();
				File stitched = new File(v.getVideoURL());
				GeneralUtils.openFile(stitched);

			}

			
		});
		videosInnerPanel.add(l);
	}

	public void populateExplorerVideos()
	{
		 List<Segment> slist = currentProject.getOrderedSegmentList();
		for(Segment s:slist)
		{
			if(s.getVideo()!=null)
			{
				makeExplorerVideo(s.getVideo());
			}
		}
		videosPanel.revalidate();
		videosPanel.repaint();
	}
public void populateExplorerOutput() {
		
	String img=currentProject.getSlideSegment(0).getSlide().getImageURL();
	if(new File(img).exists())
	{	// Show in explore
		JLabel l = new JLabel(currentProject.getProjectName());
		l.setName(currentProject.getProjectName());
		
			l.setIcon(UIUtils.getThumbnail(img));
		l.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (Desktop.isDesktopSupported()) {

					String f = e.getComponent().getName();
					String fileoutput =new File(currentProject.getProjectURL(),currentProject.getProjectName()+".mp4").getAbsolutePath();
					File stitched = new File(fileoutput);
					if(stitched.exists())
					{
						Desktop desktop = Desktop.getDesktop();
						try {
							desktop.open(stitched);
						} catch (java.io.IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}

			}
		});
		outputInnerPanel.add(l);
		outputPanel.revalidate();
		outputPanel.repaint();
	}
	}

	public void populateExplorerSlides() {
		currentProject = ProjectService.getInstance(currentProject.getProjectJsonPath());
		List<Segment> slist = currentProject.getOrderedSegmentList();
		int displayIndex=0;
		for(Segment s:slist)
		{
			if(s.getSlide()!=null)
			{
				addExplorerSlide(s.getSlide(),displayIndex);
				displayIndex++;
			}
		}
		presentationPanel.revalidate();
		presentationPanel.repaint();
	}
	

	public void createProject() {
		clean();
		populateProject();
	}

	public void repopulateProject()
	{
		clean();
		populateProject();
	}
	public void populateProject() {
		System.out.println("Calling populate");
		populateExplorer();
		populateTimeline();

	}

	public void populateExplorer() {
		populateExplorerSlides();
		populateExplorerVideos();
		populateExplorerOutput();
	}

	public void populateTimeline() {
		currentProject = ProjectService.getInstance(currentProject.getProjectJsonPath());
		List<Segment> slist = currentProject.getOrderedSegmentList();
		System.out.println("sList size: "+slist.size());
		customPanelList.clear();
		int slideIndex=0,videoIndex=1;
		x=0;
		for(Segment s:slist)
		{
			if(s.getSlide()!=null)
			{
				JComponent p=makeTimelineSlide(s.getSlide(),slideIndex,s);
				timelineInnerPanel.add(p);
				slideIndex++;
			}
			else if(s.getVideo()!=null)
			{
				JComponent p=makeTimelineVideo(s.getVideo(),videoIndex,s);
				timelineInnerPanel.add(p);
				videoIndex++;
			}
			x+=142;
		}
		timelineFrame.getContentPane().revalidate();
		timelineFrame.getContentPane().repaint();
	}

	private int roundUp(int n) {
		if ((n % 2) != 0) {
			return (n + 1);
		}
		return n;
	}

	public void setCoordinates(int rectx, int recty, int rectwidth, int rectheight) {
		x = roundUp(rectx);
		y = roundUp(recty);
		recordingWidth = rectwidth;
		recordingHeight = rectheight;
	}

	public void disable() {
		notesFrame.setEnabled(false);
		explorerFrame.setEnabled(false);
		timelineFrame.setEnabled(false);
		slideFrame.setEnabled(false);
		presentationPanel.setEnabled(false);
		mnEdit.setEnabled(false);
		mnImport.setEnabled(false);
		 mnOptions.setEnabled(false);
		mnExport.setEnabled(false);
		mntmVideoFormat.setEnabled(false);
		mntmAudioFormat.setEnabled(false);
		mntmDecideRecordingArea.setEnabled(false);
		btnRecord.setEnabled(false);
		btnScreenRec.setEnabled(false);
		stopbtn.setEnabled(false);
		btnNext.setEnabled(false);
		btnDiscard.setEnabled(false);
		mntmFormat.setEnabled(false);
	}

	public void enable() {
		// System.out.print(b);
		notesFrame.setEnabled(true);
		explorerFrame.setEnabled(true);
		timelineFrame.setEnabled(true);
		slideFrame.setEnabled(true);
		presentationPanel.setEnabled(true);
		//mnEdit.setEnabled(true);
		mnImport.setEnabled(true);
		mnOptions.setEnabled(true);
		mnExport.setEnabled(true);
		mntmVideoFormat.setEnabled(true);
		mntmAudioFormat.setEnabled(true);
		mntmDecideRecordingArea.setEnabled(true);
		btnRecord.setEnabled(true);
		btnScreenRec.setEnabled(true);
	}

	private void launchPresentation(String pptPath) {

		try {

			File rootPath = new File(System.getProperty("java.class.path"));
			System.out.println("The root path is " + rootPath);
			String openOfficePath = GeneralUtils.findOooPath();
			if (openOfficePath!=null) {
				LibreDesktop.launchOfficeInstance(pptPath, openOfficePath);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}



	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Workspace frame = new Workspace();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void addExplorerSlide(com.iitb.lokavidya.core.data.Slide slide,int index) {
		
		JLabel label = new JLabel();

		label.setName(Integer.toString(index));
		label.setIcon(UIUtils.getThumbnail(slide.getImageURL()));

		label.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				try
				{
				paused=false;
				currentSlideNumber = Integer.parseInt(e.getComponent().getName());
				if(btnNext.isEnabled()){
				System.out.println("Found " + e.getComponent().getName());
				
				if(screenRecordingFlag)
				{
					WorkspaceUIHelper.disableRecord();
					Thread.sleep(1600);
					po.stopToggleSlideRecording(currentProject);
					setPreview(e.getComponent().getName());
					po.startToggleSlideRecording(currentProject, currentProject.getSlideSegment(currentSlideNumber));
				}
				else if(!btnRecord.isEnabled())
				{
					WorkspaceUIHelper.disableRecord();
					Thread.sleep(1600);
					ProjectOperations.stopSlideRecording();
					setPreview(e.getComponent().getName());
					ProjectOperations.startSlideRecording(currentProject, currentProject.getSlideSegment(currentSlideNumber));
				}
				}
				else
					setPreview(e.getComponent().getName());
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			}	
			
		});
		presentationInnerPanel.add(label);
		presentationPanel.revalidate();
		presentationPanel.repaint();
	}

	public void removeExplorerSlides() {
		Component[] a = presentationInnerPanel.getComponents();
		for (Component j : a) {
			presentationInnerPanel.remove(j);
		}
	}

	public void removeExplorerVideos() {
		// Remove explorer videos
		Component[] a = videosInnerPanel.getComponents();
		for (Component j : a) {
			videosInnerPanel.remove(j);
		}
		// remove from individual recordings
		
		
	}

	public void removeExplorerOutput() {
		Component[] a = outputInnerPanel.getComponents();
		for (Component j : a) {
			outputInnerPanel.remove(j);
		}
	}

	public void clean() {
		removeExplorer();
		removeTimeline();
	}

	public void removeExplorer() {
		removeExplorerSlides();
		removeExplorerVideos();
		removeExplorerOutput();
	}

	public void removeTimeline() {
		Component[] a = timelineInnerPanel.getComponents();
		for (Component j : a) {
			timelineInnerPanel.remove(j);
		}
	
	}

	

	public static void setPreview(String slidename) {
		int index=Integer.parseInt(slidename);
		System.out.println("Showing slide "+index);
		Segment s=currentProject.getSlideSegment(index);
		currentSegment =s;
		lblSlideDisplay.setIcon(UIUtils.getPreview(s.getSlide().getImageURL()));
		Call.workspace.revalidate();
		Call.workspace.repaint();
	}

	protected static ImageIcon createImageIcon(String path) {
		//java.net.URL imgURL = Workspace.class.getResource(path);
		//if (imgURL != null) {
			//return new ImageIcon(imgURL);
		
		ImageIcon p=null;
		URL l = Workspace.class.getResource(path);
		
		if(l!=null)
		{
			p=new ImageIcon(l);
			System.out.println("Found image resource");
			return p;
		}
		if(new File(path).exists()){
			return new ImageIcon(path);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	protected JComponent makePresPanel() {
		presentationInnerPanel = new JPanel(false);
		presentationInnerPanel.setBackground(new Color(245, 245, 220));
		presentationInnerPanel.setLayout(new GridLayout(0, 1, 10, 10));

		JScrollPane scrollPane = new JScrollPane();
		// presentationPanel.add(scrollPane);
		scrollPane.setViewportView(presentationInnerPanel);
		scrollPane.setVisible(true);
		// panel.add(filler);
		return scrollPane;
	}

	protected JComponent makeVideosPanel() {
		videosInnerPanel = new JPanel(false);
		videosInnerPanel.setBackground(new Color(245, 245, 220));
		videosInnerPanel.setLayout(new GridLayout(0, 1, 10, 10));

		JScrollPane scrollPane = new JScrollPane();
		// presentationPanel.add(scrollPane);
		scrollPane.setViewportView(videosInnerPanel);
		scrollPane.setVisible(true);
		// panel.add(filler);
		return scrollPane;
	}

	public JComponent makeTimelineSlide(com.iitb.lokavidya.core.data.Slide slide, int index, Segment segment) {
	//	JPanel piece = new JPanel(false);
		System.out.println("Slide Url: "+slide.getImageURL());
		CustomPanel piece = new CustomPanel(segment);
        String slidename="Slide "+Integer.toString(index+1);
      
        JLabel l1=new JLabel(slidename);
        piece.springLayout.putConstraint(SpringLayout.NORTH, l1, 5, SpringLayout.NORTH, this);
        piece.springLayout.putConstraint(SpringLayout.WEST, l1, 7, SpringLayout.WEST, this);
        //piece.springLayout.putConstraint(SpringLayout.SOUTH, l1, 34, SpringLayout.NORTH, this);
        piece.springLayout.putConstraint(SpringLayout.EAST, l1, 133, SpringLayout.WEST, this);
        JLabel l2=new JLabel();
        l2.setName(Integer.toString(index));
        l2.setIcon(UIUtils.getThumbnail(slide.getImageURL()));
        l2.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				try {
					paused=false;
					currentSlideNumber = Integer.parseInt(e.getComponent().getName());
					if(btnNext.isEnabled()){
						
						if(screenRecordingFlag)
						{
							WorkspaceUIHelper.disableRecord();
							Thread.sleep(1600);
							po.stopToggleSlideRecording(currentProject);
							setPreview(e.getComponent().getName());
							po.startToggleSlideRecording(currentProject, currentProject.getSlideSegment(currentSlideNumber));
						}
						else if(!btnRecord.isEnabled())
						{
							WorkspaceUIHelper.disableRecord();
							Thread.sleep(1600);
							ProjectOperations.stopSlideRecording();
							setPreview(e.getComponent().getName());
							ProjectOperations.startSlideRecording(currentProject, currentProject.getSlideSegment(currentSlideNumber));
						}
					}
					else
						setPreview(e.getComponent().getName());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
        piece.springLayout.putConstraint(SpringLayout.NORTH, l2, 4, SpringLayout.SOUTH, l1);
        piece.springLayout.putConstraint(SpringLayout.WEST, l2, 0, SpringLayout.WEST, l1);
        JButton b3=new JButton("Edit slide");
        b3.setBackground(SystemColor.control);
        b3.setFont(new Font("Tahoma", Font.PLAIN, 10));
        b3.setPreferredSize(new Dimension(10, 10));
        b3.setName(Integer.toString(index));
        //b3.setMinimumSize(new Dimension(15, 28));
        piece.springLayout.putConstraint(SpringLayout.NORTH, b3, 90, SpringLayout.SOUTH, l1);
        piece.springLayout.putConstraint(SpringLayout.WEST, b3, 0, SpringLayout.WEST, l1);
        piece.springLayout.putConstraint(SpringLayout.EAST, b3, -5, SpringLayout.EAST, l1);
        piece.springLayout.putConstraint(SpringLayout.SOUTH, b3, -10, SpringLayout.SOUTH, piece);
        b3.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.out.println("Segment slide is "+e.getComponent().getName());
				Segment s=currentProject.getSlideSegment(Integer.parseInt(e.getComponent().getName()));
				SegmentService.convertPresentation(s, currentProject);
				GeneralUtils.stopOfficeInstance();
				launchPresentation(s.getSlide().getPptURL());
				refreshList.add(s);
			}
		});
        JLabel l4=new JLabel();
        l4.setName("Audio");
        l4.setFont(new Font("Tahoma", Font.PLAIN, 10));
        piece.springLayout.putConstraint(SpringLayout.NORTH, l4, 0, SpringLayout.NORTH, l2);
        piece.springLayout.putConstraint(SpringLayout.EAST, l4, +85, SpringLayout.EAST, l2);
        
        JButton b5=new JButton("Play");
        b5.setName("Play+"+Integer.toString(index));
        b5.setBackground(SystemColor.control);
        b5.setFont(new Font("Tahoma", Font.PLAIN, 10));
        piece.springLayout.putConstraint(SpringLayout.NORTH, b5, 10, SpringLayout.SOUTH, l4);
        piece.springLayout.putConstraint(SpringLayout.WEST, b5, -63, SpringLayout.EAST, l4);
        piece.springLayout.putConstraint(SpringLayout.EAST, b5, 0, SpringLayout.EAST, l4);
        
        b5.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				String str=e.getComponent().getName().replace("Play+", "");
				Segment s=currentProject.getSlideSegment(Integer.parseInt(str));
				if(s.getSlide().getAudio()!=null)
				{
					File f=new File(s.getSlide().getAudio().getAudioURL());
					GeneralUtils.openFile(f);
				}
				else if(s.getVideo()!=null)
				{
					File f=new File(s.getVideo().getVideoURL());
					GeneralUtils.openFile(f);
				}
			}
		});
        JButton b6=new JButton("Delete");
        b6.setName(Integer.toString(index));
        b6.setBackground(SystemColor.control);
        b6.setFont(new Font("Tahoma", Font.PLAIN, 10));
        piece.springLayout.putConstraint(SpringLayout.NORTH, b6, 6, SpringLayout.SOUTH, b5);
        piece.springLayout.putConstraint(SpringLayout.WEST, b6, -63, SpringLayout.EAST, l4);
        piece.springLayout.putConstraint(SpringLayout.EAST, b6, 0, SpringLayout.EAST, l4);
        b6.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.out.println("Registered delete click");
				 Container parent = e.getComponent().getParent();
				 for(Component x:parent.getComponents())
				 {
					 try {
						if (x.getName().equals("Audio")) {
							((JLabel) x)
									.setIcon(createImageIcon("resources/mic_off.png"));
							((JLabel) x).setText("No audio");
						}
						if (x.getName().contains("Play")) {
							x.setEnabled(false);
						}
					} catch (Exception e2) {
						// TODO: handle exception
						e2.printStackTrace();
					}
				 }
				 e.getComponent().setEnabled(false);
				 parent.revalidate();
				 parent.repaint();
				Segment s=currentProject.getSlideSegment(Integer.parseInt(e.getComponent().getName()));
				
				SegmentService.deleteAudio(currentProject,s);
				SegmentService.deleteVideo(currentProject,s);
			}
		});
        
        
        if(slide.getAudio()!=null)
        {
        	System.out.println("Could find slide audio "+slide.getAudio().getAudioURL());
        	l4.setIcon(createImageIcon("resources/mic.png"));
        	l4.setText("Has Audio");
        	b5.setEnabled(true);
        	b6.setEnabled(true);
        }
        else if(segment.getVideo()!=null)
        {
        	l4.setIcon(createImageIcon("resources/slidevideo.png"));
        	l4.setText("Has Video");
        	b5.setEnabled(true);
        	b6.setEnabled(true);
        }
    	else
    	{	
    		System.out.println("Couldn't find slide audio");
        	l4.setIcon(createImageIcon("resources/mic_off.png"));
        	l4.setText("No audio");
        	b5.setEnabled(false);
        	b6.setEnabled(false);
        	
        	//l3.setFont(font);
    	}
        
        piece.add(l1);
        piece.add(l2);
        piece.add(b3);
        piece.add(l4);
        piece.add(b5);
        piece.add(b6);
        //piece.setName(slidename);
        
        customPanelList.add(piece);
        return piece;
	}

	
	
	
	
	protected JComponent makeTimelineVideo(com.iitb.lokavidya.core.data.Video v,int index, Segment segment) {
		CustomPanel piece = new CustomPanel(segment);

		JLabel l1 = new JLabel("Video "+Integer.toString(index));
		JLabel l2 = new JLabel();
		
		
		l2.setIcon(createImageIcon("resources/individual_video.png"));
		
		piece.springLayout.putConstraint(SpringLayout.NORTH, l1, 5, SpringLayout.NORTH, this);
        piece.springLayout.putConstraint(SpringLayout.WEST, l1, 7, SpringLayout.WEST, this);
        //piece.springLayout.putConstraint(SpringLayout.SOUTH, l1, 34, SpringLayout.NORTH, this);
        piece.springLayout.putConstraint(SpringLayout.EAST, l1, 133, SpringLayout.WEST, this);
        
        piece.springLayout.putConstraint(SpringLayout.NORTH, l2, 4, SpringLayout.SOUTH, l1);
        piece.springLayout.putConstraint(SpringLayout.WEST, l2, 0, SpringLayout.WEST, l1);

        piece.add(l1);
        piece.add(l2);

        customPanelList.add(piece);
		return piece;
	}

	

	protected JComponent makeOutputPanel() {
		outputInnerPanel = new JPanel(false);
		outputInnerPanel.setBackground(new Color(245, 245, 220));
		outputInnerPanel.setLayout(new GridLayout(0, 1, 0, 0));

		JScrollPane scrollPane = new JScrollPane();
		// presentationPanel.add(scrollPane);
		scrollPane.setViewportView(outputInnerPanel);
		scrollPane.setVisible(true);
		// panel.add(filler);
		return scrollPane;
	}

	public static void stopRecording() {
		paused=false;
		if(screenRecordingFlag)
		{
			screenRecordingFlag=false;
			po.stopToggleSlideRecording(currentProject);
			recFrame.hideFrame();
			
		}
		else
		{
			ProjectOperations.stopSlideRecording();
			playing=false;
		}
		ProjectService.persist(currentProject);
		Call.workspace.removeTimeline();
		Call.workspace.populateTimeline();
		WorkspaceUIHelper.disableStop();
	}
	public static void discardRecording() {
		paused=false;
		if(screenRecordingFlag)
		{
			screenRecordingFlag=false;
			po.discardToggleSlideRecording(currentProject);
			recFrame.hideFrame();
			
		}
		else
		{
			ProjectOperations.discardSlideRecording(currentProject);
			playing=false;
		}
		ProjectService.persist(currentProject);
		Call.workspace.removeTimeline();
		Call.workspace.populateTimeline();
		WorkspaceUIHelper.disableStop();
	}
	/**
	 * Create the frame.
	 */
	public Workspace() {
		//recording = new Recording();
		// selfRef=this;
		
		customPanelList= new ArrayList<CustomPanel>();
		refreshList=new ArrayList<Segment>();
		deleteList=new ArrayList<File>();
		executedActions=new Stack<State>();
		redoActions=new Stack<State>();
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1210, 710);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		/*File Menu */
		mnFile = new JMenu("File");
		menuBar.add(mnFile);
        //New Project button
		mntmNewMenuItem = new JMenuItem("New Project");
		mnFile.add(mntmNewMenuItem);
		mntmNewMenuItem.setToolTipText("Create an new project");
		mntmNewMenuItem.setAccelerator(
				javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Remove later: CreateProject.workspacestatic = selfRef;
				initCustomPanel();                                            //New Project pop-up invoke
				CreateProject.main(null);
				System.out.println("Enable called");
				// enable();
			}
		});
		//Open Project button
		mntmOpenMenuItem = new JMenuItem("Open Project");
		mnFile.add(mntmOpenMenuItem);
		mntmOpenMenuItem.setToolTipText("Open Existing project from the computer");
		mntmOpenMenuItem.setAccelerator(
				javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
		mntmOpenMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				initCustomPanel();                                           //Open Project pop-up invoke
				try {
					OpenProject.main(null);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				
			}
		});
		// Exit Button in file
		 mntmClose = new JMenuItem("Close");
		 mntmClose.setAccelerator(KeyStroke.getKeyStroke(
		             KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		 mntmClose.setToolTipText("Exit application");
		 mntmClose.addActionListener(new ActionListener() {
		 public void actionPerformed(ActionEvent event) {
		            System.exit(0);
		        }

		    });
		    mnFile.add(mntmClose);
		    
		/*Edit Menu */   
		mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		/* Options Menu */
		mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);
		/* Import Menu */
		mnImport = new JMenu("Import");
		mntmPresentation = new JMenuItem("Presentation ...");
		mntmVideo = new JMenuItem("Video ...");
		mntmAndroid = new JMenuItem("Android Project ...");
		mnImport.add(mntmPresentation);
		mnImport.add(mntmVideo);
		mnImport.add(mntmAndroid);
		menuBar.add(mnImport);

		mntmPresentation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Called open");
				try {
					   customPanelList.clear();
					   CustomPanel.highlightCount=0;
					   CustomPanel.slideCount =0;
					   CustomPanel.selected[0]=null;
					   CustomPanel.selected[1]=null;
					   OpenPresentation.main(null);
					   /*pptPath=new DirectoryChooser(GeneralUtils.getDocumentsPath(),"ppt").selectedfile;
					   System.out.println("Being executed");
					   if(pptPath.equals(""))
						{
							System.out.println("Path null");
							JOptionPane.showMessageDialog(null, "Enter the presentation location", "", JOptionPane.INFORMATION_MESSAGE);
						}
						else
						{
							ProjectService.importPresentation(pptPath,currentProject);
						}*/	
				} catch (NullPointerException ex) {
					// logger.log(Level.SEVERE, e.getMessage(), e);
					ex.printStackTrace();
				}
			}
		});

		mntmVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				customPanelList.clear();
				   CustomPanel.highlightCount=0;
				   CustomPanel.slideCount =0;
				   CustomPanel.selected[0]=null;
				   CustomPanel.selected[1]=null;
				   OpenVideo.main(null);

			}
		});

		mntmAndroid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				customPanelList.clear();
				   CustomPanel.highlightCount=0;
				   CustomPanel.slideCount =0;
				   CustomPanel.selected[0]=null;
				   CustomPanel.selected[1]=null;
				OpenAndroid.main(null);
			//	setPaths();

			}
		});

		mntmVideoFormat = new JMenuItem("Video Format");
		mnOptions.add(mntmVideoFormat);
		mntmVideoFormat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VideoFormat.main(null);
			}
		});

		
		mntmAudioFormat = new JMenuItem("Audio Format");
		mnOptions.add(mntmAudioFormat);
		
		

		mntmDecideRecordingArea = new JCheckBoxMenuItem("Decide Recording Area");
		mnOptions.add(mntmDecideRecordingArea);
		mntmDecideRecordingArea.setSelected(false);
		mntmDecideRecordingArea.addItemListener(new ItemListener() {

			public void itemStateChanged(ItemEvent e) {
				if (mntmDecideRecordingArea.isSelected()) {
					setVisible(false);
					Timer t = new Timer(500 * 1, new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							// do your reoccuring task

							try {
								SelectArea.main(null);
								mntmDecideRecordingArea.setSelected(true);
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});
					t.start();
					t.setRepeats(false);
				} else {
					System.out.println("Resetting coordinates");
					x = 0;
					y = 0;
					recordingWidth = 0;
					recordingHeight = 0;
				}
			}
		});
		mnExport = new JMenu("Export");
		menuBar.add(mnExport);

		mntmFormat = new JMenuItem("Video Format");
		mnExport.add(mntmFormat);

		mntmAndroidexp = new JMenuItem("Android App");
		mnExport.add(mntmAndroidexp);
		mntmAndroidexp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				OpenAndroidExp.main(null);

			}
		});
		
		mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		mntmFeedback = new JMenuItem("Feedback");
		mnHelp.add(mntmFeedback);
		
		mntmFeedback.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Feedback.main(null);
					
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
			}
		});
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		// contentPane.setBounds(0, 0, 1300, 780);
		setContentPane(contentPane);

		explorerFrame = new JInternalFrame("Project Explorer");
		explorerFrame.getContentPane().setBackground(new Color(139, 176, 244));// ,149,237));
		explorerFrame.setBounds(5, 5, 300, 412);
		explorerFrame.setVisible(true);

		slideFrame = new JInternalFrame("Recorder");
		slideFrame.setBounds(315, 5, 587, 412);
		slideFrame.getContentPane().setBackground(Color.WHITE);
		SpringLayout springLayout_1 = new SpringLayout();
		slideFrame.getContentPane().setLayout(springLayout_1);

		panel = new JPanel();
		springLayout_1.putConstraint(SpringLayout.NORTH, panel, -43, SpringLayout.SOUTH, slideFrame.getContentPane());
		springLayout_1.putConstraint(SpringLayout.WEST, panel, 0, SpringLayout.WEST, slideFrame.getContentPane());
		springLayout_1.putConstraint(SpringLayout.SOUTH, panel, 0, SpringLayout.SOUTH, slideFrame.getContentPane());
		springLayout_1.putConstraint(SpringLayout.EAST, panel, 592, SpringLayout.WEST, slideFrame.getContentPane());
		panel.setBackground(new Color(220, 220, 220));
		slideFrame.getContentPane().add(panel);
		SpringLayout sl_panel = new SpringLayout();
		panel.setLayout(sl_panel);

		btnRecord = new JButton("");
		sl_panel.putConstraint(SpringLayout.NORTH, btnRecord, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, btnRecord, 10, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, btnRecord, -7, SpringLayout.SOUTH, panel);
		sl_panel.putConstraint(SpringLayout.EAST, btnRecord, -537, SpringLayout.EAST, panel);
		btnRecord.setSelectedIcon(createImageIcon("resources/record.png"));
		//URL st = Workspace.class.getResource("");
		//System.out.println(st.getPath());
		//btnRecord.setSelectedIcon(new ImageIcon());
		btnRecord.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnRecord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Gif.main(null);
				if(playing &&(!paused))
				{
					paused=true;	
					btnRecord.setIcon(createImageIcon("resources/record.png"));
					//playing=false;
				}
				else if(playing && paused)
				{
					paused=false;
					btnRecord.setEnabled(true);
					btnRecord.setIcon(createImageIcon("resources/pause.png"));
				}
				else 
				{
					playing = true;
					if (currentSlide == null)
						setPreview(Integer.toString(currentSlideNumber));
					/* TODO pass selected segment */
					/* Place holder Segment *///currentSegment = new Segment(currentProject.getProjectURL());
					ProjectOperations.startSlideRecording(currentProject,
							currentSegment);
					WorkspaceUIHelper.disableRecord();
					
					System.out.println("outside thread,,,");
					
				}

			}
		});
		btnRecord.setIcon(createImageIcon("resources/record.png"));
		btnRecord.setBackground(new Color(176, 196, 222));
		panel.add(btnRecord);
		lblSlideDisplay = new JLabel("");
		lblSlideDisplay.setHorizontalAlignment(SwingConstants.CENTER);
		springLayout_1.putConstraint(SpringLayout.NORTH, lblSlideDisplay, 0, SpringLayout.NORTH,
				slideFrame.getContentPane());
		springLayout_1.putConstraint(SpringLayout.WEST, lblSlideDisplay, 0, SpringLayout.WEST,
				panel);
		springLayout_1.putConstraint(SpringLayout.SOUTH, lblSlideDisplay, -6, SpringLayout.NORTH, panel);
		springLayout_1.putConstraint(SpringLayout.EAST, lblSlideDisplay, 0, SpringLayout.EAST,
				slideFrame.getContentPane());

		stopbtn = new JButton("");
		sl_panel.putConstraint(SpringLayout.NORTH, stopbtn, 0, SpringLayout.NORTH, btnRecord);
		sl_panel.putConstraint(SpringLayout.SOUTH, stopbtn, -7, SpringLayout.SOUTH, panel);
		stopbtn.setSelectedIcon(createImageIcon("resources/stop.png"));
		stopbtn.setIcon(createImageIcon("resources/stop.png"));
		stopbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Thread.sleep(1600);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//recording.stopRecording();
				stopRecording();
			}
		});
		stopbtn.setFont(new Font("Tahoma", Font.PLAIN, 11));

		stopbtn.setBackground(new Color(176, 196, 222));
		panel.add(stopbtn);

		btnNext = new JButton("");
		sl_panel.putConstraint(SpringLayout.EAST, stopbtn, -277, SpringLayout.WEST, btnNext);
		sl_panel.putConstraint(SpringLayout.WEST, btnNext, -156, SpringLayout.EAST, panel);
		sl_panel.putConstraint(SpringLayout.EAST, btnNext, -111, SpringLayout.EAST, panel);
		btnNext.setIcon(createImageIcon("resources/navigate.png"));
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*
				 * for(Slide libraryslide: librarySlides) {
				 * if(libraryslide.getName().equals(currentSlide.getName())) {
				 * int index=librarySlides.indexOf(libraryslide);
				 * if(index+1<librarySlides.size()) {
				 * System.out.println(librarySlides.get(index+1).getName());
				 * setPreview(librarySlides.get(index+1).getName());
				 * recording.stopSlide(); recording.startSlide(); } else {
				 * recording.stopSlide(); } break; }
				 * 
				 * }
				 */
				changeSlideRight();
			}
		});
		sl_panel.putConstraint(SpringLayout.NORTH, btnNext, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, btnNext, -7, SpringLayout.SOUTH, panel);
		btnNext.setSelectedIcon(createImageIcon("resources/navigate.png"));
		btnNext.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnNext.setBackground(new Color(176, 196, 222));
		panel.add(btnNext);
		
		btnDiscard = new JButton("");
		btnDiscard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				discardRecording();
			}
		});
		sl_panel.putConstraint(SpringLayout.NORTH, btnDiscard, 0, SpringLayout.NORTH, btnRecord);
		sl_panel.putConstraint(SpringLayout.WEST, btnDiscard, 6, SpringLayout.EAST, btnNext);
		sl_panel.putConstraint(SpringLayout.SOUTH, btnDiscard, 0, SpringLayout.SOUTH, btnRecord);
		sl_panel.putConstraint(SpringLayout.EAST, btnDiscard, 51, SpringLayout.EAST, btnNext);
		btnDiscard.setFont(new Font("Dialog", Font.PLAIN, 11));
		btnDiscard.setEnabled(false);
		btnDiscard.setIcon(createImageIcon("resources/discard.png"));
		btnDiscard.setBackground(new Color(176, 196, 222));
		panel.add(btnDiscard);
		
		btnScreenRec = new JButton("");
		sl_panel.putConstraint(SpringLayout.EAST, btnScreenRec, -484, SpringLayout.EAST, panel);
		sl_panel.putConstraint(SpringLayout.WEST, stopbtn, 6, SpringLayout.EAST, btnScreenRec);
		btnScreenRec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(screenRecordingFlag &&(!paused))
				{
					paused=true;	
					btnScreenRec.setIcon(createImageIcon("resources/videocam.png"));
					//playing=false;
				}
				else if(screenRecordingFlag && paused)
				{
					paused=false;
					btnScreenRec.setEnabled(true);
					btnScreenRec.setIcon(createImageIcon("resources/pause.png"));
				}
				else {
					if (currentSlide == null)
						setPreview(Integer.toString(currentSlideNumber));
					/* TODO pass selected segment */
					/* Place holder Segment *///currentSegment = new Segment(currentProject.getProjectURL());
					recFrame = new RecordingFrame(x, y, recordingWidth,
							recordingHeight);
					po = new ProjectOperations();
					po.startToggleSlideRecording(currentProject, currentSegment);
					screenRecordingFlag = true;
					WorkspaceUIHelper.disableRecord();
				}
			}
		});
		btnScreenRec.setEnabled(false);
		btnScreenRec.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnScreenRec.setIcon(createImageIcon("resources/videocam.png"));
		btnScreenRec.setSelectedIcon(createImageIcon("resources/videocam.png"));
		btnScreenRec.setBackground(new Color(176, 196, 222));
		sl_panel.putConstraint(SpringLayout.NORTH, btnScreenRec, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, btnScreenRec, 6, SpringLayout.EAST, btnRecord);
		sl_panel.putConstraint(SpringLayout.SOUTH, btnScreenRec, -7, SpringLayout.SOUTH, panel);
		panel.add(btnScreenRec);
		
		btnNewButton_2 = new JButton("New button");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startOperation();
			}
		});
		sl_panel.putConstraint(SpringLayout.NORTH, btnNewButton_2, 0, SpringLayout.NORTH, btnRecord);
		sl_panel.putConstraint(SpringLayout.WEST, btnNewButton_2, 25, SpringLayout.EAST, stopbtn);
		panel.add(btnNewButton_2);
		
		lblSlideDisplay.setIcon(createImageIcon("resources/start.jpg"));
		slideFrame.getContentPane().add(lblSlideDisplay);
		slideFrame.setVisible(true);

		tabbedPane = new JTabbedPane();
		tabbedPane.setBounds(10, 22, 264, 349);
		tabbedPane.setBackground(Color.LIGHT_GRAY);
		ImageIcon icon = createImageIcon("images/middle.gif");
		contentPane.setLayout(null);
		explorerFrame.getContentPane().setLayout(null);

		presentationPanel = makePresPanel();
		tabbedPane.addTab("Presentation", icon, presentationPanel, "Edit Presentation in LibreOffice");
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
		// tabbedPane.addTab("Presentation", icon, panel2,"Does nothing");
		explorerFrame.getContentPane().add(tabbedPane);

		videosPanel = makeVideosPanel();
		tabbedPane.addTab("Videos", icon, videosPanel, "View videos");
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_2);
		// tabbedPane.addTab("Presentation", icon, panel2,"Does nothing");
		explorerFrame.getContentPane().add(tabbedPane);

		outputPanel = makeOutputPanel();
		tabbedPane.addTab("Output", icon, outputPanel, "View output video");
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_3);
		// tabbedPane.addTab("Presentation", icon, panel2,"Does nothing");
		explorerFrame.getContentPane().add(tabbedPane);

		contentPane.add(explorerFrame);

		contentPane.add(slideFrame);

		timelineFrame = new JInternalFrame("Video Outline");
		timelineFrame.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		timelineFrame.getContentPane().setBackground(new Color(176, 224, 230));
		timelineFrame.getContentPane().setLayout(null);

		timelineScroll = new JScrollPane();
		timelineScroll.setBounds(12, 12, 1030, 159);
		timelineScroll.setBackground(SystemColor.control);
		timelineScroll.setHorizontalScrollBarPolicy(
				   JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		timelineInnerPanel = new JPanel();
		// timelineInnerPanel.setBounds(12, 12, 1144, 159);
		timelineInnerPanel.setBackground(SystemColor.control);
		timelineScroll.setViewportView(timelineInnerPanel);
		timelineInnerPanel.setLayout(new GridLayout(1, 0, 10, 10));
		//timelineInnerPanel.setBounds(0, 0, 5000, 158);
		timelineScroll.setVisible(true);

		timelineFrame.getContentPane().add(timelineScroll);

		JPanel StitchToolbarpanel = new JPanel();
		StitchToolbarpanel.setBackground(SystemColor.control);
		StitchToolbarpanel.setBounds(1055, 12, 101, 159);
		timelineFrame.getContentPane().add(StitchToolbarpanel);
		StitchToolbarpanel.setLayout(null);

		btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setEnabled(false);
				for(Segment s:refreshList)
					if(s.getSlide()!=null){
						s.getSlide().refresh();
					}
				refreshList.clear();
				Call.workspace.removeTimeline();
				Call.workspace.populateTimeline();
				Call.workspace.removeExplorerSlides();
				Call.workspace.populateExplorerSlides();
				setEnabled(true);
			}
		});
		btnRefresh.setIcon(null);
		btnRefresh.setBounds(2, 10, 96, 19);
		btnRefresh.setFont(new Font("Dialog", Font.BOLD, 10));
		btnRefresh.setBackground(new Color(245, 245, 245));
		StitchToolbarpanel.add(btnRefresh);

		btnSwap = new JButton("Exchange");
		btnSwap.setIcon(null);
		
		btnSwap.setBounds(2, 110, 96, 19);
		btnSwap.setFont(new Font("Dialog", Font.BOLD, 10));
		btnSwap.setBackground(new Color(245, 245, 245));
		btnSwap.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if(CustomPanel.highlightCount == 2) {
					System.out.println("Swap time..");
					System.out.println("1: "+CustomPanel.selected[0].getSegment().getId() + " 2: "+CustomPanel.selected[1].getSegment().getId());
					System.out.println("Size: "+customPanelList.size());

					CustomPanel panel1 = CustomPanel.selected[0];
					CustomPanel panel2 = CustomPanel.selected[1];
					panel1.isHighlighted=false;
					panel2.isHighlighted=false;
					panel1.setBorder(panel1.blackBorder);
					panel2.setBorder(panel2.blackBorder);
					
					currentProject.swapSegment(CustomPanel.selected[0].getSegment(), CustomPanel.selected[1].getSegment());
					CustomPanel.selected[0]= null;
					CustomPanel.selected[1] = null;
					CustomPanel.highlightCount=0;
					CustomPanel.slideCount=0;
					customPanelList.clear();
					removeTimeline();
					populateTimeline();
					System.out.println("swap complete");
					System.out.println("Sequence: \n");
					Iterator <Integer> iterator =  currentProject.getOrderingSequence().iterator();
					while(iterator.hasNext())
						System.out.print(iterator.next()+", ");
					Call.workspace.removeTimeline();
					Call.workspace.populateTimeline();
					Call.workspace.removeExplorerSlides();
					Call.workspace.populateExplorerSlides();
			}
			}	
		});
		
		
		
		StitchToolbarpanel.add(btnSwap);

		btnDelete = new JButton("Delete");
		btnDelete.setIcon(null);
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				removeTimeline();
				CustomPanel.highlightCount=0;
				
				ArrayList<CustomPanel> selected=new ArrayList<CustomPanel>();
				
				for(CustomPanel c: customPanelList)
				{
					if(c.isHighlighted)
					{
						c.isHighlighted=false;
						c.setBorder(c.blackBorder);
						currentProject.deleteSegment(c.getSegment());
						selected.add(c);
					}
				}
				for(CustomPanel c: selected)
				{
					customPanelList.remove(c);
				}
				selected.clear();
				/*Iterator <CustomPanel> iterator = customPanelList.iterator();
				while(iterator.hasNext()){
					JPanel panel = iterator.next();
					timelineInnerPanel.add(panel);
				}
				
				//populateTimeline();
				timelineFrame.getContentPane().revalidate();
		        timelineFrame.getContentPane().repaint();*/
		        Call.workspace.removeTimeline();
				Call.workspace.populateTimeline();
				Call.workspace.removeExplorerSlides();
				Call.workspace.populateExplorerSlides();
		        CustomPanel.selected[0] = null;
			}
		});
		btnDelete.setBounds(2, 135, 96, 19);
		btnDelete.setBackground(new Color(245, 245, 245));
		btnDelete.setFont(new Font("Dialog", Font.BOLD, 10));
		StitchToolbarpanel.add(btnDelete);
		
		

		btnStitch = new JButton("Create");
		btnStitch.setIcon(null);
		btnStitch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//recording.stitch();
				currentProject = ProjectService.getInstance(currentProject.getProjectJsonPath());
				if(currentProject != null)
				{
					ProjectOperations.stitch(currentProject);
					showOutput();
				}
			}
		});
		btnStitch.setBounds(2, 35, 96, 19);
		btnStitch.setFont(new Font("Dialog", Font.BOLD, 10));
		btnStitch.setBackground(new Color(245, 245, 245));
		StitchToolbarpanel.add(btnStitch);
		
		btnSaveOrder = new JButton("Save");
		btnSaveOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ProjectService.persist(currentProject);
			}
		});
		btnSaveOrder.setIcon(null);
		btnSaveOrder.setBounds(2, 60, 96, 19);
		btnSaveOrder.setFont(new Font("Dialog", Font.BOLD, 10));
		btnSaveOrder.setBackground(new Color(245, 245, 245));
		StitchToolbarpanel.add(btnSaveOrder);

		btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Segment s=new Segment(currentProject.getProjectURL());
				currentProject.addSegment(s);
				ProjectService.persist(currentProject);
				repopulateProject();
			}
		});
		btnAdd.setFont(new Font("Dialog", Font.BOLD, 10));
		btnAdd.setBackground(new Color(245, 245, 245));
		btnAdd.setBounds(2, 85, 96, 19);
		StitchToolbarpanel.add(btnAdd);

		timelineFrame.setBounds(10, 428, 1174, 211);
		contentPane.add(timelineFrame);
		// Speaker's Notes Frame
		notesFrame = new JInternalFrame("Speaker's Notes");
		notesFrame.getContentPane().setBackground(new Color(176, 196, 222));
		notesFrame.setBounds(916, 5, 268, 412);
		contentPane.add(notesFrame);
		notesFrame.setVisible(true);
		timelineFrame.setVisible(true);
		
		final SpringLayout springLayout = new SpringLayout();
		notesFrame.getContentPane().setLayout(springLayout);
		pathDef=GeneralUtils.getDocumentsPath();
		// Jlabel for Upload Notes from 
		JLabel lblNewLabel_1 = new JLabel("Upload Notes From:");																
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_1, 10, SpringLayout.NORTH, notesFrame.getContentPane());
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		notesFrame.getContentPane().add(lblNewLabel_1);
		// JTextField for path of upload
		textField = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, textField, 124, SpringLayout.WEST, notesFrame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, textField, -10, SpringLayout.EAST, notesFrame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_1, -6, SpringLayout.WEST, textField);
		springLayout.putConstraint(SpringLayout.NORTH, textField, -2, SpringLayout.NORTH, lblNewLabel_1);
		textField.setText(pathDef);
		notesFrame.getContentPane().add(textField);
		textField.setColumns(10);
		
		// Browse Button
		btnNewButton = new JButton("Browse..");
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 6, SpringLayout.SOUTH, lblNewLabel_1);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 0, SpringLayout.WEST, lblNewLabel_1);
	
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				path=new DirectoryChooser(pathDef,"").selectedfile;
				//System.out.println(path);
				textField.setText(path);
			}
		});
		notesFrame.getContentPane().add(btnNewButton);
		//Upload Button
		btnNewButton_1 = new JButton("Extract");
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_1, 0, SpringLayout.NORTH, btnNewButton);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton_1, 0, SpringLayout.EAST, textField);
		notesFrame.getContentPane().add(btnNewButton_1);
		notesArea = new JTextArea();
		springLayout.putConstraint(SpringLayout.NORTH, notesArea, 9, SpringLayout.SOUTH, btnNewButton);
		springLayout.putConstraint(SpringLayout.WEST, notesArea, 10, SpringLayout.WEST, notesFrame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, notesArea, -10, SpringLayout.SOUTH, notesFrame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, notesArea, 0, SpringLayout.EAST, textField);
		notesArea.setVisible(true);
		notesFrame.getContentPane().add(notesArea);
		
		this.disable();
		this.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	GeneralUtils.stopOfficeInstance();
		    	for(Segment s:refreshList)
					if(s.getSlide()!=null){
						s.getSlide().refresh();
					}
				refreshList.clear();
				for(File f: deleteList)
				{
					f.delete();
				}
		    }
		});
	}
}