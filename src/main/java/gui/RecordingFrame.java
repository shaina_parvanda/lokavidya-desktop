/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author Joey
 */
public class RecordingFrame {
   public static  Window w;
    public RecordingFrame(final int startx,final int starty,final int width,final int height) {

       w=new Window(null)
		{ 
		  @Override 
		  public void paint(Graphics g)
		  { 
		    final Font font = getFont().deriveFont(48f);
		    g.setFont(font);
		    g.setColor(Color.RED);
		    
		    g.drawRect(startx,starty,width,height);
		  } 
		  @Override 
		  public void update(Graphics g)
		  { 
		    paint(g);
		  } 
		}; 
	
		w.setAlwaysOnTop(true);
		w.setBounds(w.getGraphicsConfiguration().getBounds());
		w.setBackground(new Color(0, true));
  }
    
    public void showFrame(){
        w.setVisible(true); 
    }
    public void hideFrame(){
        w.setVisible(false); 
    }
   
  
    public static void main(String args[])
    {
    	RecordingFrame r=new RecordingFrame(0,0,800,600);
    	r.showFrame();
    	//r.decideFrame();
    }
}
