package com.iitb.lokavidya.core.operations;

import gui.Call;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.swing.JComponent;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;

import poi.PptToImages;
import poi.PptxToImages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.iitb.lokavidya.core.data.Project;
import com.iitb.lokavidya.core.data.Segment;
import com.iitb.lokavidya.core.data.Slide;
import com.iitb.lokavidya.core.utils.FFMPEGWrapper;
import com.iitb.lokavidya.core.utils.GeneralUtils;

public class ProjectService {

	private static List<String> pptPaths=new ArrayList();
	public static Project createNewProject(String projectURL)
	{
		if(new File(projectURL).isDirectory())
			try {
				System.out.println("Deleting");
				FileUtils.cleanDirectory(new File(projectURL+File.separator));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else
		{
			System.out.println("Returning null");
			return null;
		}

		Project project = new Project();
		String projectName = new File(projectURL).getName();
		System.out.println(projectName);
		project.setProjectName(projectName);
		System.out.println(projectURL);
		project.setProjectURL(projectURL);

		//List<Segment> segmentList = new ArrayList<Segment>();
		List<Integer> orderingSequence = new ArrayList<Integer>();
		project.setOrderingSequence(orderingSequence);
		
		Segment segment = new Segment(projectURL);
		project.addSegment(segment);
		
		ProjectService.persist(project);
		GeneralUtils.stopOfficeInstance();
		return project;

	}
	
	public static Project getInstance(String pathToProjectJson)
    {
    	System.out.println("Retrieving instance from JSON: Start");
    	
    	
    	Project project = null;
        Gson gson = new Gson();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToProjectJson));
            project = gson.fromJson(bufferedReader, new TypeToken<Project>() {
            }.getType());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Retrieving instance from JSON:END");
        
        try{
	    	FileInputStream fis = new FileInputStream(pathToProjectJson);
	    	String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
	    	fis.close();
	    	if(!md5.equals(FileUtils.readFileToString(new File(FilenameUtils.concat(project.getProjectURL(), project.getProjectName()+".hash")), "UTF-8")))
	    	{
	    		System.out.println("Corrupt JSON");
	    		return null;
	    	}
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
        
        File jsonFile = new File(pathToProjectJson);
        File projectFolder=jsonFile.getParentFile();
        String oldprojectPath=project.getProjectURL();
        if(!projectFolder.getAbsolutePath().equals(oldprojectPath))
	    {
        	System.out.println("Changing");
        	project.setProjectURL(projectFolder.getAbsolutePath());
	        List<Segment> segmentList = project.getOrderedSegmentList();
	        for(int i=0;i<segmentList.size();i++)
	        {
	        	if (segmentList.get(i).getSlide()!=null)
	        	{
	        		segmentList.get(i).getSlide().setImageURL(segmentList.get(i).getSlide().getImageURL().replace(oldprojectPath, projectFolder.getAbsolutePath()));
	        		segmentList.get(i).getSlide().setPptURL(segmentList.get(i).getSlide().getPptURL().replace(oldprojectPath, projectFolder.getAbsolutePath()));
	        		if(segmentList.get(i).getSlide().getAudio()!=null)
	        		{
		        		segmentList.get(i).getSlide().getAudio().setAudioURL(segmentList.get(i).getSlide().getAudio().getAudioURL().replace(oldprojectPath, projectFolder.getAbsolutePath()));
	        		}
	        	}
	        	if (segmentList.get(i).getVideo()!=null)
	        	{
	        		segmentList.get(i).getVideo().setVideoURL(segmentList.get(i).getVideo().getVideoURL().replace(oldprojectPath, projectFolder.getAbsolutePath()));
	        	}
	        	
	        }
	    }
        ProjectService.persist(project);
        return project;
    }
	
	public static void persist(Project project)
	{
		File jsonFile= new File(FilenameUtils.concat(project.getProjectURL(), project.getProjectName()+".json"));
		if(!jsonFile.exists())
		{
			try {
				jsonFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();	
			}
		}
    	try {    
			Writer writer = new FileWriter(jsonFile.getAbsolutePath());
            Gson gson = new GsonBuilder().create();
            gson.toJson(project, writer);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    	
    	try{
	    	FileInputStream fis = new FileInputStream(FilenameUtils.concat(project.getProjectURL(), project.getProjectName()+".json"));
	    	String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
	    	fis.close();
	    	Writer writer = new FileWriter(FilenameUtils.concat(project.getProjectURL(), project.getProjectName()+".hash"));
	    	writer.write(md5);
	    	writer.close();
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
	
	public static void main(String[] args)
	{
		GeneralUtils.convertImageToPresentation("/home/frg/Documents/eighteen/ecezznvr5a.png", "/home/frg/Desktop/abc.odp");

//		System.out.println(System.getProperty("java.io.tmpdir"));
		Project test= ProjectService.getInstance("/home/frg/Documents/seven/seven.json");
		
		
//
//		
////
//		Segment segment4 = new Segment(test.getProjectURL());
//		Video video = new Video("/home/sanket/Desktop/sample.mp4",test.getProjectURL());
//		segment4.setVideo(video);
//		test.addSegment(segment4);
//		ProjectService.persist(test);
////		importAndroidProject("/home/sanket/Documents/asd/asd.json", "/home/sanket/Documents/material-required.zip");
//		System.out.println(test);
		//importPresentation("/home/sanket/Documents/Lokavidya_Desktop_Application using LibreOffice.pptx", test);
		//SegmentService.deleteImage(segment);
		//ProjectService.persist(test);
		//ProjectService.exportAndroidProject("/home/sanket/Documents/asd", "/home/sanket/Documents");
	//	ProjectService.importAndroidProject("/home/sanket/Documents/pazx/pazx.json", "/home/sanket/Documents/material-required.zip");
		//ProjectService.exportAndroidProject("/home/sanket/Documents/okzjxclkj", "/home/sanket/Documents");
			
	/*	Segment segment5 = new Segment(test.getProjectURL());
		video = new Video("/home/sanket/Desktop/sample.mp4",test.getProjectURL());
		System.out.println("VideoURL"+video.getVideoURL());
		segment2.setVideo(video);
		test.addSegment(segment5);*/

/*		
//		System.out.println(System.getProperty("java.io.tmpdir"));
		Project test= ProjectService.createNewProject("/home/sanket/Documents/asd");
//  	Segment segment = new Segment("/home/sanket/Documents/asd");
//		
//		Slide slide = new Slide("/home/sanket/Documents/test.png",test.getProjectURL());
//		Audio audio = new Audio("/home/sanket/Documents/testsunday/audio/testsunday.1.wav",test.getProjectURL());
//		slide.setAudio(audio);
//		List<Reference> referencesList = new ArrayList<Reference> ();
//		Reference reference = new Reference();
//		reference.setVideoID("123");
//		referencesList.add(reference);
//		segment.setReferences(new HashSet<Reference>(referencesList));
//		segment.setSlide(slide);
//		test.addSegment(segment);
//	
//		
//		
//
//		Segment segment2 = new Segment("/home/sanket/Documents/asd");
//		Video video = new Video("/home/sanket/Desktop/sample.mp4",test.getProjectURL());
//		System.out.println("VideoURL"+video.getVideoURL());
//		segment2.setVideo(video);
//		test.addSegment(segment2);
//		ProjectService.persist(test);
//		
//		slide = new Slide("/home/sanket/Documents/test.png",test.getProjectURL());
//		audio = new Audio("/home/sanket/Documents/testsunday/audio/testsunday.1.wav",test.getProjectURL());
//		slide.setAudio(audio);
//		referencesList = new ArrayList<Reference> ();
//		reference = new Reference();
//		reference.setVideoID("123");
//		referencesList.add(reference);
//		segment2.setReferences(new HashSet<Reference>(referencesList));
//		segment2.setSlide(slide);
//		test.addSegment(segment2);
//
//		Segment segment3 = new Segment("/home/sanket/Documents/asd");
//		video = new Video("/home/sanket/Desktop/sample.mp4",test.getProjectURL());
//		System.out.println("VideoURL"+video.getVideoURL());
//		segment2.setVideo(video);
//		test.addSegment(segment3);
//		
//		
//		slide = new Slide("/home/sanket/Documents/test.png",test.getProjectURL());
//		audio = new Audio("/home/sanket/Documents/testsunday/audio/testsunday.1.wav",test.getProjectURL());
//		slide.setAudio(audio);
//		referencesList = new ArrayList<Reference> ();
//		reference = new Reference();
//		reference.setVideoID("123");
//		referencesList.add(reference);
//		segment3.setReferences(new HashSet<Reference>(referencesList));
//		segment3.setSlide(slide);
//		test.addSegment(segment3);
//		
//
//		Segment segment4 = new Segment("/home/sanket/Documents/asd");
//		video = new Video("/home/sanket/Desktop/sample.mp4",test.getProjectURL());
//		System.out.println("VideoURL"+video.getVideoURL());
//		segment2.setVideo(video);
//		test.addSegment(segment4);
//		
//		Segment segment5 = new Segment("/home/sanket/Documents/asd");
//		video = new Video("/home/sanket/Desktop/sample.mp4",test.getProjectURL());
//		System.out.println("VideoURL"+video.getVideoURL());
//		segment2.setVideo(video);
//		test.addSegment(segment5);
//		
//		test.swapSegment(segment, segment2);
//		test.swapSegment(segment3, segment2);
//		test=ProjectService.getInstance("/home/sanket/Documents/asd/asd.json");
//
//		
//		video = new Video("/home/sanket/Desktop/sample.mp4",test.getProjectURL());
//		segment4.setVideo(video);
//		test.addSegment(segment4);
//		
		importPresentation("/home/sanket/Documents/Lokavidya_Desktop_Application using LibreOffice.pptx", test);
		//SegmentService.deleteImage(segment);
		//ProjectService.persist(test);
		//ProjectService.exportAndroidProject("/home/sanket/Documents/asd", "/home/sanket/Documents");
		//ProjectService.importAndroidProject("/home/sanket/Desktop", "/home/sanket/Documents/asd.zip");

>>>>>>> e136ecc1c28748abbbb0e8fedbf5338b1c4f2b38
		
	//	test.swapSegment(segment, segment2);
	//	test.swapSegment(segment3, segment2);
		//test=ProjectService.getInstance("/home/sanket/Documents/asd/asd.json");
	//	test=ProjectService.getInstance("/Users/SidRama/Documents/asd/asd.json");
		

		//SegmentService.deleteImage(segment);
		ProjectService.persist(test);
		System.out.println("Stitching now.... "); */
//		ProjectOperations.stitch(test);
//		System.out.println("Stitching done... ");
	//	ProjectService.exportAndroidProject("/home/sanket/Documents/asd", "/home/sanket/Documents");
	//	ProjectService.importAndroidProject("/home/sanket/Desktop", "/home/sanket/Documents/asd.zip");
	//	ProjectService.exportAndroidProject("/Users/SidRama/Documents/asd", "/Users/SidRama/Documents");
	//	ProjectService.importAndroidProject("/Users/SidRama/Desktop", "/Users/SidRama/Documents/asd.zip");
	

		//GeneralUtils.convertImageToPresentation("/home/sanket/Downloads/aPDxmvV_700b_v1.jpg", "/home/sanket/Documents/test.odp");
		//GeneralUtils.convertPresentationToImage("/home/sanket/Documents/test.odp", "/home/sanket/Documents/test.png");

		System.exit(0);

	}
	
	
	public static void exportAndroidProject(String projectPath,String androidProjectPath)
	{
		Project project = ProjectService.getInstance(projectPath+File.separator+FilenameUtils.getName(projectPath)+".json"); 
		String tmpPath=System.getProperty("java.io.tmpdir");
		File projectTmp = new File(tmpPath,project.getProjectName());
		FFMPEGWrapper ffmpegWrapper = new FFMPEGWrapper();
		if(projectTmp.exists())
			try {
				FileUtils.deleteDirectory(projectTmp);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		projectTmp.mkdir();
		File projectTmpImage = new File(projectTmp.getAbsolutePath(),"images");
		if(!projectTmpImage.exists())
			projectTmpImage.mkdir();
		File projectTmpAudio = new File(projectTmp.getAbsolutePath(),"audio");
		if(!projectTmpAudio.exists())
			projectTmpAudio.mkdir();
		List<Segment> segmentList=project.getOrderedSegmentList();
		int index=1;
		String wavPath = new File(projectTmpAudio,"temp.wav").getAbsolutePath();
		for (Segment s:segmentList)
		{
			System.out.println("HIT!");
			if(s.getSlide()!=null)
			{	
				try {
					FileUtils.copyFile(new File(s.getSlide().getImageURL()), new File(projectTmpImage,project.getProjectName()+"."+index+"."+FilenameUtils.getExtension(s.getSlide().getImageURL())));
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					if (s.getSlide().getAudio()!=null) {
						File file = new File(wavPath);
						if (file.exists())
							file.delete();
						ffmpegWrapper.convertMp3ToWav(s.getSlide().getAudio().getAudioURL(), wavPath);
						FileUtils.copyFile(new File(wavPath),
								new File(projectTmpAudio, project.getProjectName() + "." + index + "."
										+ FilenameUtils.getExtension(s.getSlide().getAudio().getAudioURL())));
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				index++;
			}
			
		}
		File file = new File(wavPath);
		if (file.exists()) 
			file.delete();
		GeneralUtils.createZip(projectTmp.getAbsolutePath(), androidProjectPath);
	}

	public static void importAndroidProject(String projectjsonpath,String zipPath)
	{
		Project project = getInstance(projectjsonpath);
		String tmpPath=System.getProperty("java.io.tmpdir");
		GeneralUtils.extractZip(zipPath, tmpPath);
		String projTmpDir =FilenameUtils.getBaseName(zipPath);
		File projectTmp= new File(tmpPath,projTmpDir);
		File projectTmpImage = new File(projectTmp.getAbsolutePath(),"images");
		File projectTmpAudio = new File(projectTmp.getAbsolutePath(),"audio");
		Comparator<File> fc = new Comparator<File>(){
	        public int compare(File o1, File o2) {
	            int i1,i2;
	            
	            i1= Integer.parseInt(o1.getName().split("\\.")[1]);
	            
	            i2= Integer.parseInt(o2.getName().split("\\.")[1]);
	            
	            System.out.println(""+i1+i2);
	            if(i1>i2)
	            {
	            	return 1;
	            }
	            if(i1==i2)
	            {
	            	return 0;
	            }
	            else
	            	return -1;
	        }
	    };
		
		if(projectTmpAudio.listFiles().length==projectTmpAudio.listFiles().length)
		{
			int i=0;
			String audioExtension =FilenameUtils.getExtension(projectTmpAudio.listFiles()[0].getAbsolutePath());
			String imageExtension =FilenameUtils.getExtension(projectTmpImage.listFiles()[0].getAbsolutePath());
			List<File> audioFilesList= Arrays.asList(projectTmpAudio.listFiles());
			Collections.sort(audioFilesList,fc);
			for(File f: audioFilesList)
			{
				String name= FilenameUtils.getBaseName(f.getAbsolutePath()).split("\\.")[0];
				String index=FilenameUtils.getBaseName(f.getAbsolutePath()).split("\\.")[1];
				File imageFile=  new File(projectTmpImage,name+"."+index+"."+imageExtension);
				if(imageFile.exists())
				{
					Segment segment = new Segment(imageFile.getAbsolutePath(),f.getAbsolutePath(),project.getProjectURL());
					System.out.println("URL of the presentation:"+segment.getSlide().getPptURL());
					project.addSegment(segment);
					ProjectService.persist(project);
				}
			}
		}
		GeneralUtils.stopOfficeInstance();
	}
	
	public static void importPresentation(String presentationURL, Project project)
	{
		BufferedWriter bw = null;
		//String tempPath = System.getProperty("java.io.tmpdir");
		try {
			bw=new BufferedWriter(new FileWriter("timelog.txt",true));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String tempPath = project.getProjectURL();
		File file = new File(presentationURL);
		try{
			long current_time = System.currentTimeMillis(),new_time,first_time= System.currentTimeMillis();

			
			Future<String> res = createPresentations(presentationURL, tempPath, file);
			
			XMLSlideShow ppt = new XMLSlideShow(new FileInputStream(file));
			if(presentationURL.endsWith(".pptx"))
				new PptxToImages(presentationURL,project.getProjectURL());
			else if(presentationURL.endsWith(".ppt"))
				new PptToImages(presentationURL,project.getProjectURL());

			List<XSLFSlide> slides = ppt.getSlides();
			int size=slides.size();
			List<Segment> newSegments=new ArrayList<Segment>();
			
			for(int i=0;i<size;i++)
			{
				if(Call.workspace.cancelled)
				{
					System.out.println("Alert caught before res.get");
					return;
				}
				else
				{
					String tempImgPath = new File(project.getProjectURL(),
							("img_" + (Integer.toString(i + 1)) + ".jpg"))
							.getAbsolutePath();
					Segment segment = new Segment(project.getProjectURL(),
							false);
					System.out.println("Creating" + tempImgPath);
					Slide slide = new Slide(tempImgPath,
							project.getProjectURL(), false);
					new_time = System.currentTimeMillis();
					bw.write("\nTime taken for image creation is: "
							+ (new_time - current_time));
					current_time = new_time;
					segment.setSlide(slide);
					newSegments.add(segment);
					project.addSegment(segment);
					ProjectService.persist(project);
					new_time = System.currentTimeMillis();
					bw.write("\nTime taken for persistance is: "
							+ (new_time - current_time));
					current_time = new_time;
					//pop UI
					
					
				}
				

				

			}
			
			res.get();
			GeneralUtils.stopOfficeInstance();
			String presentationName= FilenameUtils.getBaseName(presentationURL);
			int i=0;
			for(Segment s: newSegments)
			{
				String pptPath=new File(tempPath,presentationName+"."+i+"."+FilenameUtils.getExtension(presentationURL)).getAbsolutePath();
				SegmentService.addPresentation(project, s, pptPath);
				i++;
			}
			
				new_time=System.currentTimeMillis();
			bw.write("\nTime taken for updating segments: "+(new_time-current_time));
			current_time=new_time;
			bw.write("Total time: "+(current_time-first_time));
			bw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static Future<String>  createPresentations(String presentationURL,
			String tempPath, File file) throws IOException,
			FileNotFoundException {
		BufferedWriter bw = null;
		//String tempPath = System.getProperty("java.io.tmpdir");
		try {
			bw=new BufferedWriter(new FileWriter("timelog.txt",true));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		long new_time,current_time;
		XMLSlideShow ppt = new XMLSlideShow(new FileInputStream(file));
		List<XSLFSlide> slides = ppt.getSlides();
		String presentationName= FilenameUtils.getBaseName(presentationURL);
		current_time=System.currentTimeMillis();
		
		for(int i=0;i<slides.size();i++)
		{
			if(!Call.workspace.cancelled)
			{
				File tempPPTPath = new File(tempPath,presentationName+"."+i+"."+FilenameUtils.getExtension(presentationURL));
				FileUtils.copyFile(new File(presentationURL),tempPPTPath);
				new_time=System.currentTimeMillis();
				bw.write("\nTime taken for copying the ppt: "+(new_time-current_time));
				current_time=new_time;
				
				keepSlide(tempPPTPath.getAbsolutePath(), i);
				new_time=System.currentTimeMillis();
				bw.write("\nTime taken for poi to seperate the prsentation is: "+(new_time-current_time));
				current_time=new_time;
			}
			else
			{
				System.out.println("Alert caught");
				break;
			}	
		}
		//ppt.close();
		bw.close();
		Future<String> v=new Future<String>() {
			
	
			public boolean isDone() {
				// TODO Auto-generated method stub
				return false;
			}
			
	
			public boolean isCancelled() {
				// TODO Auto-generated method stub
				return false;
			}
			
	
			public String get(long timeout, TimeUnit unit) throws InterruptedException,
					ExecutionException, TimeoutException {
				// TODO Auto-generated method stub
				return null;
			}
			
		
			public String get() throws InterruptedException, ExecutionException {
				// TODO Auto-generated method stub
				return null;
			}
			
		
			public boolean cancel(boolean mayInterruptIfRunning) {
				// TODO Auto-generated method stub
				return false;
			}
		};
        return v;
	}
	
	
	public static void keepSlide(String presentationPath, int index)
	{
		File file = new File(presentationPath);
		try {
			XMLSlideShow ppt = new XMLSlideShow(new FileInputStream(file));
			System.out.println(ppt.getSlides().size());
			List<XSLFSlide> slides = ppt.getSlides();
			XSLFSlide selectesdslide= slides.get(index);
			ppt.setSlideOrder(selectesdslide, 0);
			FileOutputStream out = new FileOutputStream(file);
			ppt.write(out);
		    out.close();
		    ppt = new XMLSlideShow(new FileInputStream(file));
		    slides = ppt.getSlides();
		    int i = slides.size()-1;
			while(i>0)
			{
				ppt.removeSlide(i);
				i--;
			}
			out = new FileOutputStream(file);
			//Saving the changes to the presentation
			System.out.println(ppt.getSlides().size());
		    ppt.write(out);
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
