package com.iitb.lokavidya.core.operations;

import gui.Call;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.lang.RandomStringUtils;

import Xuggler.DecodeAndSaveAudioVideo;
import Xuggler.VidCapture;
import Xuggler.VideoCapture;

import com.iitb.lokavidya.core.data.Audio;
import com.iitb.lokavidya.core.data.Project;
import com.iitb.lokavidya.core.data.Segment;
import com.iitb.lokavidya.core.data.Video;
import com.iitb.lokavidya.core.utils.FFMPEGWrapper;
import com.iitb.lokavidya.core.utils.GeneralUtils;
import com.iitb.lokavidya.core.utils.SoundCapture;
import com.sun.star.setup.CopyFileAction;

public class ProjectOperations {
	static SoundCapture currentSound = null;
	static VideoCapture currentMuteVideo=null;
	static String tempAudioURL;
	static String tempVideoURL;
	static SoundCapture currentAudio=null;
	public static void stitch(Project project) {
		FFMPEGWrapper ffmpegWrapper = new FFMPEGWrapper();
		ArrayList<String> videoPaths = new ArrayList<String>();
		Iterator<Segment> iterator = project.getOrderedSegmentList().iterator();
		String tmpPath = System.getProperty("java.io.tmpdir");
		while (iterator.hasNext()) {
			Segment segment = iterator.next();
			// TODO Check audio format before attempting convert
			if (segment.getSlide() != null){
			if ( segment.getSlide().getAudio() != null) {
				try {
					String audioURL = segment.getSlide().getAudio()
							.getAudioURL();
					System.out.println("Audio URL: " + audioURL);
					String imageURL = segment.getSlide().getImageURL();
					System.out.println("Image URL: " + imageURL);
					String audioName = RandomStringUtils.randomAlphanumeric(10)
							.toLowerCase();
					String mp3Path = project.getProjectURL() + File.separator
							+ audioName + ".mp3";
					ffmpegWrapper.convertWavToMp3(audioURL, mp3Path);
					// Update the audio properties
					System.out.println("outside wrapper..");
					Audio tempAudio = new Audio(mp3Path,
							project.getProjectURL());
					segment.getSlide().setAudio(tempAudio);
					segment.getSlide().getAudio().setAudioProperties();
					System.out.println("audio conversion complete");
					System.out.println("audio format: "
							+ segment.getSlide().getAudio().getAudioFormat());

					System.out.println("Video creation... ");
					String tempPath = new File(tmpPath, "temp.mp4")
							.getAbsolutePath();
					System.out.println("Temp path: " + tempPath);
					Video video = new Video(project.getProjectURL());
					System.out.println("VIDEO URL: " + video.getVideoURL());
					ffmpegWrapper.stitchImageAndAudio(imageURL, segment
							.getSlide().getAudio().getAudioURL(),
							video.getVideoURL());
					video.setVideoProperties();
					segment.setVideo(video);
					
					System.out.println(video.getVideoFormat());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			} 
			if(segment.getVideo()!=null)
				videoPaths.add(segment.getVideo().getVideoURL());
		}
		System.out.println("Video list :");
		Iterator<String> iter = videoPaths.iterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}
		// Begin concatenation
		String finalPath = new File(project.getProjectURL(),
				project.getProjectName() + ".mp4").getAbsolutePath();
		try {
			ffmpegWrapper.stitchVideo(videoPaths, new File(
					"concat.txt").getAbsolutePath(), finalPath);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	static Audio globalAudio;
	static Segment segment;
	static Video globalVideo;
	public static void startSlideRecording(Project project, Segment segmentA) {
		
		SegmentService.deleteVideo(project, segmentA);
		SegmentService.deleteAudio(project, segmentA);
		segment=segmentA;
		globalAudio = new Audio(project.getProjectURL());
		currentSound = new SoundCapture(globalAudio.getAudioURL());
		currentSound.startRecording();
	}
	public void startToggleSlideRecording(Project project, Segment segmentA) {
		SegmentService.deleteVideo(project, segmentA);
		SegmentService.deleteAudio(project, segmentA);
		segment=segmentA;
		tempAudioURL=new File(project.getProjectURL(),(RandomStringUtils.randomAlphanumeric(10).toLowerCase()+".wav")).getAbsolutePath();
		tempVideoURL=new File((RandomStringUtils.randomAlphanumeric(10).toLowerCase()+".flv")).getAbsolutePath();
		currentAudio = new SoundCapture(tempAudioURL);
		currentAudio.startRecording();
		currentMuteVideo=new VideoCapture();
		currentMuteVideo.addFile(tempVideoURL);
		currentMuteVideo.start();
		globalVideo=new Video(project.getProjectURL());
	}
	public static void playSlideRecording()
	{

	}
	public static void stopPlaySlideRecording()
	{

	}

	public static void stopSlideRecording() {
		currentSound.stopRecording();
		segment.getSlide().setAudio(globalAudio);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static void discardSlideRecording(Project p) {
		currentSound.stopRecording();
		SegmentService.deleteAudio(p, segment);
	}
	public void discardToggleSlideRecording(Project project) 
	{
		SegmentService.deleteVideo(project,segment );
		SegmentService.deleteAudio(project, segment);
		currentAudio.stopRecording();
        currentMuteVideo.stop();
       
	}
	public void stopToggleSlideRecording(Project project) 
	{
		currentAudio.stopRecording();
        currentMuteVideo.stop();
        File f=new File(tempVideoURL);
        File tempVideo=new File(project.getProjectURL(),RandomStringUtils.randomAlphanumeric(10).toLowerCase()+".flv");
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        //GeneralUtils.copyFile(f, tempMuteVideo);
		//new FFMPEGWrapper().addAudioVideo(tempAudioURL , tempVideoURL, globalVideo.getVideoURL());
		System.out.println("Saving at "+f.getAbsolutePath());
		DecodeAndSaveAudioVideo.stitch(f.getAbsolutePath(),tempAudioURL,tempVideo.getAbsolutePath());
		
		DecodeAndSaveAudioVideo.convertFormat(tempVideo.getAbsolutePath(),globalVideo.getVideoURL());
		Video screenVideo = new Video(globalVideo.getVideoURL(), project.getProjectURL());
		SegmentService.addVideo(project, segment,screenVideo);
		
		
		Call.workspace.deleteList.add(f);
		Call.workspace.deleteList.add(new File(tempAudioURL));
		Call.workspace.deleteList.add(tempVideo);
	}
}
