package com.iitb.lokavidya.core.utils;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class UIUtils {

	public UIUtils() {
		// TODO Auto-generated constructor stub
	}
	public static ImageIcon getThumbnail(String path){
		Icon icon=new ImageIcon(path);
	      ImageIcon img=(ImageIcon)icon;
	      int height=icon.getIconHeight(),width=icon.getIconWidth();
	      while(width>125){
	    	  height=height*(110)/width;
	    	  width=110;
	      }
	      Image scaleImage = img.getImage().getScaledInstance(width, height,Image.SCALE_DEFAULT);
	     return new ImageIcon(scaleImage);
	    }
	
	    public static ImageIcon getSmallImage(String path){
	    	Icon icon=new ImageIcon(path);
	        ImageIcon img=(ImageIcon)icon;
	        int height=icon.getIconHeight(),width=icon.getIconWidth();
	        while(width>125){
	      	  height=height*(70)/width;
	      	  width=70;
	        }
	        Image scaleImage = img.getImage().getScaledInstance(width, height,Image.SCALE_DEFAULT);
	       return new ImageIcon(scaleImage);
	      }
	    public static ImageIcon getPreview(String path){
	    	Icon icon=new ImageIcon(path);
	      ImageIcon img=(ImageIcon)icon;
	      int height=icon.getIconHeight(),width=icon.getIconWidth();
	      while(width>480){
	          height=height*(480)/width;
	          width=480;
	      }
	      Image scaleImage = img.getImage().getScaledInstance(width, height,Image.SCALE_DEFAULT);
	     return new ImageIcon(scaleImage);
	    }

	    public static BufferedImage createBufferedImage(String imgPath){
	    	File img = new File(imgPath);
	      BufferedImage image =  new BufferedImage(800, 600, BufferedImage.TYPE_3BYTE_BGR);
	          

	            try {  
	                image = ImageIO.read(img );
	                 image=resize(image,800, 600); 
	            }  
	            catch (Exception e) {
	            e.printStackTrace();
	            }
	            return image;
	           
	    }
	    
	    public static BufferedImage resize(BufferedImage img, int newW, int newH) { 
	    Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
	    BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

	    Graphics2D g2d = dimg.createGraphics();
	    g2d.drawImage(tmp, 0, 0, null);
	    g2d.dispose();

	    return dimg;
	} 
	

}
