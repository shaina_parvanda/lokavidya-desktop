package com.iitb.lokavidya.core.utils;

import gui.Call;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;


class StreamGobbler extends Thread {
	InputStream is;
	String type;

	StreamGobbler(InputStream is, String type) {
		this.is = is;
		this.type = type;
	}

	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null)
				System.out.println(type + ">" + line);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}

public class FFMPEGWrapper {

	String pathExecutable;

	public static void main(String args[]) {
		FFMPEGWrapper wrapper = new FFMPEGWrapper();
		/*
		 * try { wrapper.convertWavToMp3("/home/frg/Documents/sample1.wav",
		 * "/home/frg/Documents/sample1.mp3");
		 * 
		 * } catch (IOException | InterruptedException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */
		ArrayList<String> vList = new ArrayList<String>();

		vList.add("C:\\Users\\hp\\Documents\\RainWaterTank-Marathi\\goqm1hgnpa.wav");
		vList.add("C:\\Users\\hp\\Documents\\RainWaterTank-Marathi\\n6xhgpxwfy.wav");

		wrapper.standardizeResolution(vList);
		wrapper.standardizeFps(vList);
		try {
			wrapper.stitchVideo(vList, "C:\\Users\\hp\\Documents\\temp.txt",
					"C:\\Users\\hp\\Documents\\join.wav");
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public FFMPEGWrapper() {
		String osname = System.getProperty("os.name");
		System.out.println(osname);

		if (osname.contains("Windows")) {
			System.out.println("Setting to Windows");
			pathExecutable = new File(
					new File(new File(new File("resources").getAbsolutePath(),
							"ffmpeg"), "bin").getAbsolutePath(), "ffmpeg.exe")
					.getAbsolutePath();
		} else {

			System.out.println("Setting to Linux");
			// pathExecutable = new File(new
			// File(sourcePath,"ffmpeg").getAbsolutePath(),"ffmpeg").getAbsolutePath();
			pathExecutable = new File(
					new File(new File("resources").getAbsolutePath(), "bin")
							.getAbsolutePath(),
					"ffmpeg").getAbsolutePath();
			System.out.println(pathExecutable);
		}
	}

	public  boolean encodeImages(String folderPath, String tempPath,String fps)
	{
		//ffmpeg -framerate 1/5 -i img%03d.png -c:v libx264 -r 30 -pix_fmt yuv420p out.mp4
		String imgPath=new File(folderPath,"img%03d.png").getAbsolutePath();
		String[] command = new String[] {
		pathExecutable, "-framerate", fps, "-i", imgPath, "-c:v", "libx264", "-r",
		"30", "-pix_fmt", "yuv420p", "-vf", "scale=320:240", tempPath };
				
		return GeneralUtils.runProcess(command);
		
	}
	//ffmpeg -loop 1 -i img.jpg -i audio.wav -c:v libx264 -c:a aac -strict experimental -b:a 192k -shortest out.mp4
	public boolean stitchImageAndAudio(String imgPath, String audioPath,
			String videoPath) throws IOException,
			InterruptedException {
		/*
		 * Check if final or temp exists delete if it does.
		 */
		System.out.println("stitching audio");
		
		File video = new File(videoPath);
		if (video.exists())
			video.delete();
		String resolution="scale="+Integer.toString(Call.workspace.videoWidth)+":"+Integer.toString(Call.workspace.videoHeight);
		String[] command = new String[] {
				pathExecutable, "-loop", "1", "-i", imgPath,"-i",audioPath,"-c:v", "libx264", "-c:a",
						"aac", "-strict","experimental","-pix_fmt", "yuv420p", "-vf", resolution, "-b:a", "192k","-shortest",videoPath };
				
		return	GeneralUtils.runProcess(command);
	}	
	
	public boolean stitchImageAndAudio1(String imgPath, String audioPath,
			String videoPath, String tempPath) throws IOException,
			InterruptedException {
		/*
		 * Check if final or temp exists delete if it does.
		 */
		System.out.println("stitching audio");
		File temp = new File(tempPath);
		File video = new File(videoPath);
		if (temp.exists())
			temp.delete();
		if (video.exists())
			video.delete();
		String cmd = " " + pathExecutable;// +" -help";
		String cmdext = " -loop 1 -i " + imgPath
				+ " -c:v libx264 -t 2 -pix_fmt yuv420p -vf scale=320:240 "
				+ tempPath;
		cmd += cmdext;
		System.out.println("The cmd for image stitching is: " + cmd);

		String resolution="scale="+Integer.toString(Call.workspace.videoWidth)+":"+Integer.toString(Call.workspace.videoHeight);
		String[] command = new String[] {
		pathExecutable, "-loop", "1", "-i", imgPath, "-c:v", "libx264", "-t",
				"2", "-pix_fmt", "yuv420p", "-vf", resolution, tempPath };
		
		GeneralUtils.runProcess(command);
		cmd =" "+pathExecutable;//+" -help";
		cmdext=" -i "+audioPath+" -i "+tempPath+" -c:a copy -vcodec copy -strict -2 "+videoPath;
		cmd +=cmdext;
		String [] commandAudio = new String [] {
				pathExecutable,
				"-i",
				audioPath,
				"-i",
				tempPath,
				"-c:a", 
				"copy",
				"-vcodec",
				"copy",
				"-strict",
				"-2",
				videoPath
		};
		File file = new File(videoPath);
		if(file.exists())
			file.delete();

		System.out.println("audio stitiching in progress");
		System.out.println("The cmd for audio stitching is: " + cmd);
		return GeneralUtils.runProcess(commandAudio);
	}

	public boolean stitchVideo(List<String> videoPaths, String listfilePath,
			String finalPath) throws IOException, InterruptedException {
		// Writing files to order.
		System.out.println(listfilePath);
		System.out.println(finalPath);
		File file = new File(listfilePath);
		if (file.exists())
			file.delete();
		File orderVideoFile = new File(listfilePath);
		try {
			FileOutputStream out = new FileOutputStream(orderVideoFile);
			for (int i = 0; i < videoPaths.size(); i++) {
				String data = "file '" + videoPaths.get(i) + "'\n\r";
				out.write(data.getBytes());
				System.out.println(data);
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}


			String[] command = new String[] { pathExecutable, "-f", "concat",
					"-i", orderVideoFile.getAbsolutePath(), "-codec", "copy",
					finalPath };
			file = new File(finalPath);
			System.out.println(orderVideoFile.getAbsolutePath());
			if (file.exists())
				file.delete();
			String cmd = pathExecutable;// +" -help";
			String cmdext = " -f concat -i " + orderVideoFile.getAbsolutePath()
					+ " -codec copy  " + finalPath + "";
			cmd += cmdext;
			GeneralUtils.runProcess(command);
			System.out.println("stitch done.. ");

		return true;

	}

	public boolean processVideo(String videoPath) {
		String tmpPath = System.getProperty("java.io.tmpdir");
		System.out.println("Video Path: " + videoPath);
		String tempAudioOutput = new File(tmpPath, "tempAudio.mp3")
				.getAbsolutePath();
		System.out.println("Temp output: " + tempAudioOutput);

		File file = new File(tempAudioOutput);
		if (file.exists())
			file.delete();

		String[] commandAudio = new String[] { pathExecutable, "-i", videoPath,
				tempAudioOutput };
		GeneralUtils.runProcess(commandAudio);
		String tempVideoOutput = new File(tmpPath, "tempVideo.mp4")
				.getAbsolutePath();
		file = new File(tempVideoOutput);
		if (file.exists())
			file.delete();
		String[] commandVideo = new String[] { pathExecutable, "-i", videoPath,
				"-vcodec", "copy", "-an", tempVideoOutput };
		GeneralUtils.runProcess(commandVideo);
		String[] commandVideoCombine = new String[] { pathExecutable, "-i",
				tempAudioOutput, "-i", tempVideoOutput, "-c:a", "copy",
				"-vcodec", "copy", "-strict", "-2", videoPath };
		file = new File(videoPath);
		if (file.exists())
			file.delete();
		System.out.println("Video Combine part command: "
				+ commandVideoCombine);
		GeneralUtils.runProcess(commandVideoCombine);
		return true;
	}

	public boolean standardizeResolution(List<String> videoPaths) {
		for (String vPath : videoPaths) {
			String cmd = " " + pathExecutable;// +" -help";
			String cmdext = " -i " + vPath
					+ " -s 720x480 -b 512k -vcodec copy -acodec copy";
			cmd += cmdext;
			Runtime run = Runtime.getRuntime();
			Process pr;
			try {
				pr = run.exec(cmd);
				pr.waitFor();
				BufferedReader buf = new BufferedReader(new InputStreamReader(
						pr.getInputStream()));
				String line = "";
				while ((line = buf.readLine()) != null) {
					System.out.println(line);
				}
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		// ffmpeg -i <inputfilename> -s 640x480 -b 512k -vcodec mpeg1video
		// -acodec copy
		return true;
	}

	public boolean standardizeResolutionMaintainAspectRatio(String videoURL,
			String projectURL, String videoName) {

		System.out.println("resizing... ");
		System.out.println("video path: " + videoURL);
		String outputPath = new File(projectURL, videoName + "."           //whwhwhwwhhwh 
				+ FilenameUtils.getExtension(videoURL)).getAbsolutePath(); //323232332232
		
		String resolution="[in]scale=iw*min("+Integer.toString(Call.workspace.videoWidth)+"/iw\\,"+Integer.toString(Call.workspace.videoHeight)+"/ih):ih*min("+Integer.toString(Call.workspace.videoWidth)+"/iw\\,"+Integer.toString(Call.workspace.videoHeight)+"/ih)[scaled]; [scaled]pad="+Integer.toString(Call.workspace.videoWidth)+":"+Integer.toString(Call.workspace.videoHeight)+":("+Integer.toString(Call.workspace.videoWidth)+"-iw*min("+Integer.toString(Call.workspace.videoWidth)+"/iw\\,"+Integer.toString(Call.workspace.videoHeight)+"/ih))/2:("+Integer.toString(Call.workspace.videoHeight)+"-ih*min("+Integer.toString(Call.workspace.videoWidth)+"/iw\\,"+Integer.toString(Call.workspace.videoHeight)+"/ih))/2[padded]; [padded]setsar=1:1[out]";
		String[] command = new String[] {
				pathExecutable,
				"-y",
				"-i",
				videoURL,
				"-vf",
				resolution,
				"-c:v", "libx264", "-c:a", "copy", outputPath };

		String newcmd = pathExecutable
				+ " -y -i "
				+ videoURL
				+ " -vf '[in]scale=iw*min(320/iw\\,240/ih):ih*min(320/iw\\,240/ih)[scaled]; [scaled]pad=320:240:(320-iw*min(320/iw\\,240/ih))/2:(240-ih*min(320/iw\\,240/ih))/2[padded]; [padded]setsar=1:1[out]' -c:v libx264 -c:a copy "
				+ outputPath;
		System.out.println("newcmd: " + newcmd);
		return GeneralUtils.runProcess(command);

	}

	public boolean standardizeFps(List<String> videoPaths) {
		for (String vPath : videoPaths) {
			String cmd = " " + pathExecutable;// +" -help";
			File f1 = new File(new File(vPath).getParent(), "temp.mp4");
			String cmdext = " -i " + vPath + " -sameq -r 4 -y "
					+ f1.getAbsolutePath();
			cmd += cmdext;
			Runtime run = Runtime.getRuntime();
			Process pr;
			try {

				pr = run.exec(cmd);
				pr.waitFor();
				BufferedReader buf = new BufferedReader(new InputStreamReader(
						pr.getInputStream()));
				String line = "";
				while ((line = buf.readLine()) != null) {
					System.out.println(line);
					new File(vPath).delete();
					f1.renameTo(new File(vPath));
				}
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return true;
	}

	public boolean convertMp3ToWav(String mp3Path, String wavPath) {
		// ffmpeg -i 111.mp3 -acodec pcm_s16le -ac 1 -ar 16000 out.wav
		String[] command = new String[] { pathExecutable, "-i", mp3Path,
				"-acodec", "pcm_s16le", "-ac", "1", "-ar", "16000", wavPath };
		File file = new File(wavPath);
		if (file.exists())
			file.delete();
		Runtime run = Runtime.getRuntime();
		Process pr;
		try {
			pr = run.exec(command);
			pr.waitFor();
			StreamGobbler errorGobbler = new StreamGobbler(pr.getErrorStream(),
					"ERROR");
			StreamGobbler outputGobbler = new StreamGobbler(
					pr.getInputStream(), "OUTPUT");
			errorGobbler.start();
			outputGobbler.start();
			int exitVal = pr.waitFor();
			System.out.println("ExitValue: " + exitVal);
			System.out.println("Wait for has ended ");
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		return true;
	}

	public boolean convertWavToMp3(String wavPath, String mp3Path)
			throws IOException, InterruptedException {
		String cmd = " " + pathExecutable;// +" -help";
		System.out.println("WAV PATH: " + wavPath);
		System.out.println("mp3PATH: " + mp3Path);
		String cmdext = " -i " + wavPath + " -f mp2 " + mp3Path;
		cmd += cmdext;
		String[] command = new String[] { pathExecutable, "-i", wavPath, "-f",
				"mp2", mp3Path };
		File file = new File(mp3Path);
		if (file.exists()) {
			file.delete();

		}
		System.out.println("Audio conversion command: " + command.toString()
				+ "\n" + cmd);
		return GeneralUtils.runProcess(command);
	}

	
}
