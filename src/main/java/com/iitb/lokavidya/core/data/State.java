package com.iitb.lokavidya.core.data;

import gui.Call;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class State {

	Project initialState;
	List<File> filesCreated,filesDeleted;
	public State(State st) {
		// TODO Auto-generated constructor stub
		setInitialState(st.getInitialState());
		setFilesCreated(st.getFilesCreated());
		setFilesDeleted(st.getFilesDeleted());
	}
	
	public State() {
		// TODO Auto-generated constructor stub
		initialState=new Project(Call.workspace.currentProject);	
		filesCreated=new ArrayList<File>();
		filesDeleted=new ArrayList<File>();
	}
	public Project getInitialState() {
		return initialState;
	}
	public void setInitialState(Project initialState) {
		this.initialState = initialState;
	}
	public List<File> getFilesCreated() {
		return filesCreated;
	}
	public void setFilesCreated(List<File> filesCreated) {
		this.filesCreated = filesCreated;
	}
	public List<File> getFilesDeleted() {
		return filesDeleted;
	}
	public void setFilesDeleted(List<File> filesDeleted) {
		this.filesDeleted = filesDeleted;
	}
	
	public void addFile(String fileName)
	{
		filesCreated.add(new File(fileName));
	}
	public void removeFile(String fileName)
	{
		filesDeleted.add(new File(fileName));
	}
	
}
